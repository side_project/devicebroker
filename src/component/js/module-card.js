'use strict';

import baseFcn from '../../script/asset/base-fcn.js';
import modules from '../../script/asset/modules/modules.js';

import Emitter from '../../script/asset/com/emitter.js';
import Receiver from '../../script/asset/com/receiver.js';

import paramsForm from '../../component/params-form.vue';
import setterForm from '../../component/setter-form.vue';


const self = {
  props: {
    path: {
      type: String,
      default: '',
    },
  },
  components: {
    'params-form': paramsForm,
    'setter-form': setterForm,
  },
  data: function () {
    return {
      status: { // card 狀態
        isSelected: false, // 已選擇
        isPortOpen: false, // COM 是否開啟
        isRecordLoading: true, // 讀取紀錄中

        showOption: false, // 顯示 option
      },

      com: { // serialport 相關參數
        path: 'COM3', // 路徑
        obj: null, // serialport 物件
        parser: null, // serialport 解析器
        emitter: new Emitter(), // 發射器
        receiver: new Receiver(), // 接收器

        option: { // 預設設定值
          baudRate: 115200,
          highWaterMark: 65536,
          dataBits: 8,
          stopBits: 1,
          parity: 'none',

          sendClear: false, // 發送後自動清空
          autoScroll: true, // 自動捲動
          clearQueueWhenNoRes: true, // 沒有響應時，清空命令駐列
        },
      },

      module: { // 模組相關參數
        type: '', // 類型
        id: '', // 型號
        ver: '', // 版本
        panel: '', // 面板類型

        params: { // 目前已接收到的參數
          /* data: {
            timestamp: 0, val: 0x03
          }*/
        },
      },

      setter: { // 儲存 setter-form 數值
        value: [], // 變數儲存
        // computed: [],       // 顯示清單
      },

      input: {
        timestamp: 0,
        val: '',
      },

      textarea: { // 接收訊息顯示區
        display: { // 顯示方式
          val: 'time',
          dateFormat: 'MM-DD HH:mm:ss',
        },
        style: {
          fontSize: { // 文字大小
            val: 14, unit: 'px',
          },
          lineHeight: { // 行高
            val: 2, unit: '',
          },
          letterSpacing: { // 字距
            val: 1, unit: 'px',
          },
        },
        datas: [], // 提示框資料
      },

      debounce: { // 避免 this 指向問題，debounce fcn 存在變數中
        settingEvent: null,
      },

      quasar: { // quasar 組件
        select: {
          module: { // input event filter 使用
            type: '',
            id: '',
            ver: '',
          },
        },
      },
    };
  },
  beforeCreate: function () {
  },
  created: function () {
    this.com.path = this.path;

    // 建立 debounce function
    this.debounce.settingEvent = baseFcn.debounce((cardSetting) => {
      this.$emit('setting', cardSetting);
    }, 1000);

    // 網頁環境
    if (baseFcn.isWeb()) {
      this.module.type = 'zigbee';
      this.module.id = 'E180-ZG120A';
      this.module.ver = '1.0.0';
      this.module.panel = 'coordinator';

      this.status.isRecordLoading = false;
      return;
    }

    // 自動載入 card 記錄檔
    this.$loadRecord(`card-${this.path}`).then((setting) => {
      // console.log('[ load card setting ] setting : ', setting);
      this.status.isRecordLoading = false;
      this.setCardSetting(setting);
    }).catch((err) => {
      this.showTips(`${this.path} Card 載入設定檔錯誤 : ${err}`, 'err');
    });
  },
  mounted: function () {
  },
  beforeDestroy: function () {
    if (this.com.obj && this.com.obj.isOpen && this.com.obj.close) {
      this.com.obj.close();
    }

    if (this.com.obj && this.com.obj.removeAllListeners) {
      this.com.obj.removeAllListeners();
    }

    if (this.com.parser && this.com.parser.removeAllListeners) {
      this.com.parser.removeAllListeners();
    }
  },
  watch: {
    module: function (val) {
      // console.log('module : ', val);
    },

    cardSetting: {
      handler: function (setting) {
        this.debounce.settingEvent(setting);
      },
      deep: false, immediate: false,
    },
  },
  methods: {
    // 卡片開關
    switchPort: function () {
      if (!this.status.isPortOpen) {
        if (!this.com.obj) {
          return this.createPort();
        }

        return this.com.obj.open();
      }

      if (baseFcn.isWeb()) {
        this.status.isPortOpen = false;
        return;
      }

      // 關閉 port
      if (this.com.obj && this.com.obj.isOpen) {
        // this.com.emitter.destroyPort()
        // this.com.receiver.destroyPort()

        this.com.obj.close();
      }
    },

    // 建立 port
    createPort: function () {
      if (baseFcn.isWeb()) {
        this.status.isPortOpen = true;
        return;
      }

      const SerialPort = global.SerialPort;
      if (!SerialPort) {
        return this.showTips('serialport 模組載入異常');
      }

      const portObj = new SerialPort(this.com.path, this.com.option);
      this.com.obj = portObj;
      // console.log('[ createPort ]');

      // 創建解析器
      const ByteLength = SerialPort.parsers.ByteLength;
      const parser = portObj.pipe(new ByteLength({ length: 1 }));//* /
      this.com.parser = parser;

      // 傳入 serialport 至 emitter、receiver
      this.com.emitter.setPort(portObj, parser);
      this.com.receiver.setPort(portObj, parser);

      const moduleData = {
        params: this.moduleParams, cmds: this.moduleCmds,
      };
      this.com.emitter.setModule(moduleData);
      this.com.receiver.setModule(moduleData);

      // --- event ---

      portObj.on('open', () => {
        this.status.isPortOpen = true;
        this.showTips('Port 開啟');

        /* this.com.emitter.addCmds([
          { key: 'getDeviceAllParams' },
        ])//*/
      });

      portObj.on('close', () => {
        this.status.isPortOpen = false;
        this.showTips('Port 關閉');
      });

      this.com.emitter.on('sent', (cmd) => { // 命令成功發送後
        // console.log(`[ ${this.path} emitter ] sent : `, cmd);
        this.com.receiver.emit('sent', cmd);
      });
      this.com.emitter.on('msg', (msg) => { // 訊息
        console.log(`[ ${this.path} emitter ] msg : `, msg);
      });
      this.com.emitter.on('err', (err) => { // 發生錯誤
        // console.log(`[ ${this.path} emitter ] err : `, err);
        this.showTips(err.msg, 'err');

        if (this.com.option.clearQueueWhenNoRes &&
          err.status === 'timeout') {
          this.com.emitter.clearQueue();
          this.showTips('因命令未響應，自動清除後續命令', 'err');
        }
      });


      this.com.receiver.on('raw', (raw) => { // 原始 bytes raw
        console.log(`[ ${this.path} receiver ] raw : `, raw);
      });
      this.com.receiver.on('transfer', (raw) => { // 互相傳輸資料
        console.log(`[ ${this.path} receiver ] transfer : `, raw);
        const str = raw.reduce((acc, cVal) => {
          return acc + String.fromCharCode(cVal);
        }, '');

        this.showTextareaMsg(str);
      });
      this.com.receiver.on('cmdRes', (cmd) => { // 命令發送後，成功響應
        console.log(`[ ${this.path} receiver ] cmdRes : `, cmd);
        this.showTips(`命令「${cmd.name}」響應成功`);
      });
      this.com.receiver.on('data', (data) => { // 解析後的資料
        // console.log(`[ ${this.path} receiver ] data : `, data);
        this.com.emitter.emit('data', data);
        this.saveModuleParams(data);
      });
      this.com.receiver.on('msg', (data) => { // 訊息
        console.log(`[ ${this.path} receiver ] msg : `, data);
      });
      this.com.receiver.on('err', (err) => { // 發生錯誤
        console.log(`[ ${this.path} receiver ] err : `, err);
      });


      portObj.on('error', (err) => {
        this.showTips(`Port 發生錯誤 : ${err}`, 'err');
        console.log(`${this.path} err : `, err);
      });
    },
    deleteCard: function () {
      this.$emit('delete', this.path);
    },

    // 發送 input 字串
    sendInput: function () {
      if (!this.com.obj || !this.com.obj.isOpen) {
        return this.showTips('請開啟 Port', 'err');
      }

      this.input.timestamp = new Date() / 1;
      this.com.obj.write(this.input.val);

      this.$nextTick(() => {
        this.scrollTextarea();
      });
    },

    // 接收 setter send 事件
    onSetterSend: function (cmd) {
      console.log('[ onSetterSend ] cmd : ', cmd);

      const res = this.com.emitter.addCmd(cmd.key, cmd.params, cmd.extParams);

      if (res.status !== 'sus') this.showTips(res.msg, res.status);
      // this.showTips(res.msg, res.status);
    },
    // 接收 setter msg 事件
    onSetterMsg: function (data) {
      this.showTips(data.msg, data.type);
    },
    // 接收 setter setterChange 事件
    onSetterChange: function (data) {
      // console.log('[ onSetterChange ]', data);

      const setterKeys = ['value'];
      setterKeys.forEach((key) => {
        this.setter[key] = data[key];
      });
    },

    // 儲存解析成功的模組參數
    saveModuleParams: function (params) {
      Object.keys(params).forEach((key) => {
        this.$set(this.module.params, key, {
          timestamp: baseFcn.getUnix(),
          val: params[key],
        });
      });
    },

    // 儲存接收數據
    showTextareaMsg: function (data) {
      this.textarea.datas.push({
        timestamp: baseFcn.getUnix(), data,
      });

      this.$nextTick(() => {
        this.scrollTextarea();
      });
    },
    // 在提示顯示區顯示系統提示訊息
    showTips: function (msg, type = '') {
      // console.log(`[ showTips ] `, msg);

      let data = ``;

      switch (type) {
        case 'err': case 'warn':
          data = `<span class="${type}">⊙ ${msg}</span>`;
          break;

        default: {
          data = `⊙ ${msg}`;
        }
      }

      this.textarea.datas.push({
        timestamp: baseFcn.getUnix(),
        data,
      });

      this.$nextTick(() => {
        this.scrollTextarea();
      });
    },
    formatTimestamp: function (stamp) {
      return baseFcn.formatUnix(stamp * 1000, this.textarea.display.dateFormat);
    },

    // 清空接收訊息
    clearTextarea: function () {
      this.textarea.datas = [];
      this.textarea.datas.length = 0;
    },
    // textarea 滾動到底
    scrollTextarea: function () {
      try {
        this.setDomScroll(this.$refs.dataTextarea);
      } catch (err) {
        //
      }
    },

    // select input 過濾儲存值
    qSelectInput: function (val, key) {
      this.quasar.select.module[key] = val;
    },

    // focus 卡片
    selected: function () {
      this.status.isSelected = true;
    },
    // blur 卡片
    blur: function () {
      this.status.isSelected = false;
    },

    // 控制 DOM 捲動條
    setDomScroll: function (dom, pos = - 1) {
      if (pos === -1) {
        dom.scrollTop = 99999;
        return;
      }

      dom.scrollTop = pos;
    },


    // 設定卡片設定
    setCardSetting: function (setting) {
      const cSetting = Object.copy(setting);
      delete cSetting.path;

      // 載入設定，設定值為 2 層物件
      Object.keys(cSetting).forEach((type) => {
        Object.keys(cSetting[type]).forEach((key) => {
          const data = cSetting[type][key];

          this.$set(this[type], key, data);
        });
      });
    },
    // 取得卡片設定
    getCardSetting: function () {
      return this.cardSetting;
    },
  },
  computed: {
    // textarea 顯示接收資料
    textareaString: function () {
      return this.textarea.datas.map((data) => data.data).join('');
    },
    // textarea 自訂樣式
    textareaStyle: function () {
      const style = {};

      Object.keys(this.textarea.style).forEach((key) => {
        const obj = this.textarea.style[key];
        style[key] = `${obj.val}${obj.unit}`;
      });

      return style;
    },

    // 模組設定 select 數據
    moduleSelectData: function () {
      const allMods = modules.getModules();

      // filter input
      const sType = this.quasar.select.module.type.toLowerCase();
      const sId = this.quasar.select.module.id.toLowerCase();
      const sVer = this.quasar.select.module.ver.toLowerCase();

      const data = {
        type: Object.keys(allMods),
        id: [{
          label: '請選擇模組', disable: true,
        }],
        ver: [{
          label: '請選擇型號', disable: true,
        }],
        panel: [{
          label: '請選擇版本', disable: true,
        }],
      };

      const type = this.module.type;

      // filter type
      if (sType !== '') {
        data.type = data.type.filter((item) => {
          return item.toLowerCase().includes(sType);
        });
      }


      // 未選擇模組
      if (!type) return data;

      data.id = Object.keys(allMods[type]);
      // filter id
      if (sId !== '') {
        data.id = data.id.filter((item) => {
          return item.toLowerCase().includes(sId);
        });
      }

      const id = this.module.id;

      // 未選擇型號
      if (!id) return data;

      data.ver = allMods[type][id];
      if (sVer !== '') {
        data.ver = data.ver.filter((item) => { // filter ver
          return item.toLowerCase().includes(sVer);
        });
      }

      const ver = this.module.ver;

      // 未選擇版本
      if (!ver) return data;

      const allPanel = modules.getPanel(type, id, ver);
      data.panel = Object.keys(allPanel);

      return data;
    },
    // 目前選定模組之參數清單
    moduleParams: function () {
      if (this.autoHelper.moduleDetailNotSet) {
        return {};
      }

      const type = this.module.type;
      const id = this.module.id;
      const ver = this.module.ver;

      return modules.getModuleParams(type, id, ver);
    },
    // 目前選定模組之命令集
    moduleCmds: function () {
      if (this.autoHelper.moduleDetailNotSet) {
        return {};
      }

      const type = this.module.type;
      const id = this.module.id;
      const ver = this.module.ver;

      return modules.getModuleCmds(type, id, ver);
    },

    // 目前參數面板數值
    panelVal: function () {
      if (this.autoHelper.moduleDetailNotSet) {
        return {};
      }

      const type = this.module.type;
      const id = this.module.id;
      const ver = this.module.ver;
      const panelType = this.module.panel;

      const panelDefine = modules.getPanel(type, id, ver);

      return {
        key: panelType,
        ...panelDefine[panelType],
      };
    },

    // 自動幫助功能：自動展開未完成設定項等等
    autoHelper: function () {
      const data = {
        moduleDetailNotSet: false,
      };

      if (!this.module.type || !this.module.id ||
        !this.module.ver || !this.module.panel) {
        data.moduleDetailNotSet = true;
      }

      return data;
    },

    // 卡片設定
    cardSetting: function () {
      const setting = {
        path: this.com.path,
        com: {
          option: {},
        },
        setter: {},
        module: {},
        textarea: {},
      };

      setting.com.option = Object.copy(this.com.option);

      const modKeys = ['type', 'id', 'ver', 'panel'];
      modKeys.forEach((key) => {
        setting.module[key] = this.module[key];
      });

      const textareaKeys = ['display', 'style'];
      textareaKeys.forEach((key) => {
        setting.textarea[key] = Object.copy(this.textarea[key]);
      });

      const setterKeys = ['value']; // 'computed'
      setterKeys.forEach((key) => {
        setting.setter[key] = Object.copy(this.setter[key]);
      });

      return setting;
    },
  },
};

export default self;
