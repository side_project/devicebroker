'use strict';

// import baseFcn from '../../script/asset/base-fcn.js';

const self = {
  props: {
    setter: {
      type: Object,
      required: true,
    },

    // setter 參數（來自父元件 setterList）
    computedSetterParams: {
      type: Object,
      required: true,
    },

    // setter 儲存參數（來自父元件 setters）
    setterDataParams: {
      type: Object,
      required: true,
    },
  },
  data: function () {
    return {
    };
  },
  created: function () {
    // console.log('setter               : ', this.setter);
    // console.log('computedSetterParams : ', this.computedSetterParams);
    // console.log('setterDataParams     : ', this.setterDataParams);
  },
  mounted: function () {
  },
  watch: {
  },
  methods: {
    onSetterChange: function () {
      // console.log('[ onSetterChange ] setters : ', this.setters)

      this.$emit('setterChange');
    },
  },
  computed: {
  },
};


export default self;
