'use strict';

import baseFcn from './asset/base-fcn.js';
// import modules from './asset/modules/modules.js';

import moduleCard from '../component/module-card.vue';

Quasar.lang.set(Quasar.lang.zhHant);


const vueDatas = {
  status: {
    isReady: false,
  },
  // 選單相關功能
  menu: {
    com: { // COM
      show: false,
      list: [
        {
          path: 'COM1',
          manufacturer: '',
          serialNumber: '',
          locationId: '',
          vendorId: '',
          productId: '',
        },
        {
          path: 'COM2',
          manufacturer: 'DOG',
          serialNumber: '',
          locationId: '',
          vendorId: '',
          productId: '',
        },
        {
          path: 'COM3',
          manufacturer: 'Coooooodfish',
          serialNumber: '',
          locationId: '',
          vendorId: '',
          productId: '',
        },
        {
          path: 'COM4',
          manufacturer: 'Duuuuuuck',
          serialNumber: '',
          locationId: '',
          vendorId: '',
          productId: '',
        },
        {
          path: 'COM5',
          manufacturer: 'Fuck',
          serialNumber: '',
          locationId: '',
          vendorId: '',
          productId: '',
        },
      ],
    },

    option: { // 全域設定
      show: false,
      items: {
        extFile: { // 外部檔案
          params: '',
          cmds: '',
        },

        utils: {
          variable: { // 變數功能
            store: [], // 已儲存
            selected: [], // 目標
            list: [], // 全部參數
          },
        },
      },
    },
  },


  // 現有卡片
  cards: {
    list: {
      'COM3': {
        timestamp: 0,
      },
    },
  },
};

const svm = new Vue({
  el: '.screen',
  data: vueDatas,
  components: {
    'module-card': moduleCard,
  },
  created: function () {
    if (baseFcn.isElectron()) {
      // 紀錄版本號
      const ver = require('electron').remote.app.getVersion();
      document.title = document.title + ver;

      // 清空 card
      this.cards.list = {};
    }

    this.loadNodeMods().then(() => {
      console.log('[ loadNodeMods ] 載入 node 模組成功');
      global.SerialPort.list().then((list) => {
        this.menu.com.list = list;
        console.log('[ SerialPort ] comList : ', list);
      });

      setInterval(() => {
        global.SerialPort.list().then((list) => {
          svm.$set(this.menu.com, 'list', list);
        });
      }, 3000);
    }).catch((err) => {
      console.error('[ loadNodeMods ] 載入 node 模組失敗 : ', err);

      this.$q.notify({
        type: 'negative', position: 'top',
        message: `載入 node 模組失敗`,
      });
    });

    this.registerHotKey();
  },
  mounted: function () {
    // 載入完成
    setTimeout(() => {
      this.status.isReady = true;
    }, 1000);
  },
  watch: {
  },
  methods: {
    // 切換卡片
    switchCard: function (path) {
      if (this.isCardExisting(path)) {
        this.deleteCard(path);
        return;
      }

      this.createCard(path);
    },
    // 生成卡片
    createCard: function (path) {
      this.$set(this.cards.list, path, {
        timestamp: baseFcn.getUnix(),
      });
    },
    deleteCard: function (path) {
      this.$delete(this.cards.list, path);
    },
    // 卡片是否已經存在
    isCardExisting: function (path) {
      if (this.cards.list && this.cards.list[path]) {
        return true;
      }

      return false;
    },

    // 卡片設定變更事件
    cardSettingEvent: function (setting) {
      if (baseFcn.isWeb()) return;

      // console.log(`[ cardSettingEvent ] `, setting);

      const name = `card-${setting.path}`;

      this.$saveRecord(name, setting).then((res) => {
        console.log(`[ cardSettingEvent ] ${name} 儲存成功`);
      }).catch((err) => {
        console.error(`[ cardSettingEvent ] ${name} 儲存錯誤 : `, err);
      });
    },

    // 畫面點擊事件
    screenClick: function () {
      // 取消所有卡片 selected
      Object.keys(this.cardVal).forEach((path) => {
        this.$refs[path][0].blur();
      });
    },

    // 載入 node 模組
    loadNodeMods: function () {
      return new Promise((resolve, reject) => {
        if (baseFcn.isWeb()) {
          return reject(new Error('網頁環境'));
        }

        try {
          global.SerialPort = require('serialport');
          global.EventEmitter = require('events');
          global.JsonStorage = require('electron-json-storage');
          global.FileSys = require('fs');
        } catch (err) {
          return reject(new Error(err));
        }

        resolve();
      });
    },


    // 註冊快捷鍵
    registerHotKey: function () {
      // 刪除所有卡片記錄檔
      Mousetrap.bind(['command+d', 'ctrl+d'], () => {
        this.$q.notify({
          message: '確認刪除所有卡片記錄檔？', progress: true,
          actions: [
            {
              label: '確認', color: 'yellow', icon: 'check', handler: () => {
                svm.$clearRecord();
              },
            },
            { label: '取消', color: 'white', icon: 'close' },
          ],
        });

        return false; // 阻止 browser 預設事件
      });
    },
  },
  computed: {
    // 可顯示的 COM 清單
    comList: function () {
      return this.menu.com.list.map((com) => {
        const data = {
          ...com,
          exist: false,
        };

        if (this.isCardExisting(com.path)) {
          data.exist = true;
        }

        return data;
      });
    },

    // 目前已新增卡片
    cardVal: function () {
      return this.cards.list;
    },
  },
});


window.addEventListener('keydown', function (e) {
  console.log('-------------- keydown ---------------');
  console.log('code    : ', e.code);
  console.log('key     : ', e.key);// */

  switch (e.key) {
    case '0': { // 0
      //
      break;
    }
  } // switch
});


// --- Vue 擴充 ---

// 儲存記錄
Vue.prototype.$saveRecord = (key, data) => {
  const self = Vue.prototype;

  if (baseFcn.isWeb()) return;

  if (!global || !global.JsonStorage) {
    return self.$q.notify({
      type: 'negative', position: 'top',
      message: `JsonStorage 模組載入異常`,
    });
  }

  return new Promise((resolve, reject) => {
    global.JsonStorage.set(key, data, (err) => {
      if (err) return reject(err);
      resolve();
    });
  });
};

// 讀取記錄
Vue.prototype.$loadRecord = (key) => {
  const self = Vue.prototype;

  if (baseFcn.isWeb()) return;

  if (!global || !global.JsonStorage) {
    return self.$q.notify({
      type: 'negative', position: 'top',
      message: `JsonStorage 模組載入異常`,
    });
  }

  return new Promise((resolve, reject) => {
    global.JsonStorage.get(key, (err, data) => {
      if (err) return reject(err);
      resolve(data);
    });
  });
};

// 刪除指定紀錄
Vue.prototype.$removeRecord = (key) => {
  const self = Vue.prototype;

  if (baseFcn.isWeb()) return;

  if (!global || !global.JsonStorage) {
    return self.$q.notify({
      type: 'negative', position: 'top',
      message: `JsonStorage 模組載入異常`,
    });
  }

  return new Promise((resolve, reject) => {
    global.JsonStorage.remove(key, (err) => {
      if (err) return reject(err);
      resolve();
    });
  });
};

// 清除所有紀錄
Vue.prototype.$clearRecord = (key) => {
  const self = Vue.prototype;

  if (baseFcn.isWeb()) return;

  if (!global || !global.JsonStorage) {
    return self.$q.notify({
      type: 'negative', position: 'top',
      message: `JsonStorage 模組載入異常`,
    });
  }

  return new Promise((resolve, reject) => {
    global.JsonStorage.clear(key, (err) => {
      if (err) return reject(err);

      self.$q.notify({
        type: 'positive', position: 'top',
        message: `清除所有紀錄`,
      });

      resolve();
    });
  });
};
