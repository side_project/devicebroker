/**
* serialport 接收器
*
* 接收命令
*/

import baseFcn from '../base-fcn.js';
import DataParser from './parser.js';


const receiver = function (portObj = null, options = {}) {
  let port = portObj; // serialport 物件
  let parser = null; // serialport 解析器

  let emitter = null; // EventEmitter 物件

  const timeout = options.timeout || 50; // 超時

  let pCmd = null; // 上次發送的命令
  let buffer = []; // 接收資料暫存，timeout 後才完成
  const dataParser = new DataParser(); // 命令解析器
  let module = { // 模組設定資料
    params: {}, cmds: {},
  };

  init();

  // --- fcn ---

  /** */
  function init() {
    if (baseFcn.isWeb()) return;

    if (!global.EventEmitter) {
      console.error(`[ receiver ] 無法載入 EventEmitter`);
      return;
    }

    const EventEmitter = global.EventEmitter;
    emitter = new EventEmitter();

    setEvent();
  }

  // 處理資料，超時後發出 raw emit
  const handleData = baseFcn.debounce(() => {
    emitter.emit('raw', buffer);
    buffer = []; buffer.length = 0;
  }, timeout);

  /** 解析取得的原始資料
   * @param {*} raw -
   * @return {*}
   */
  function parserRaw(raw) {
    // 解析 cmd res key
    const resArg = dataParser.parserArrayKey(pCmd.res);

    // res 沒有 key 值
    if (Object.keys(resArg.keys).length === 0) {
      return {};
    }

    // 檢查回應矩陣是否與 cmds.js 定義之 res 相等
    const isEqual = dataParser.resEqual(raw, pCmd.res);
    if (!isEqual) return {};

    return dataParser.matchKeysResData(resArg.keys, raw);
  }

  /** 建立 emitter 事件 */
  function setEvent() {
    // COM 已成功送出資料
    emitter.on('sent', (cmd) => {
      // console.log('pCmd : ', pCmd)
      pCmd = cmd;
    });

    // 已取得原始資料
    emitter.on('raw', (raw) => {
      // pCmd 存在
      if (!pCmd) {
        emitter.emit('transfer', raw);
        return console.log('[ receiver ] pCmd 不存在');
      }

      // 解析完成的數據，key-val
      const data = parserRaw(raw);
      emitter.emit('data', data);
      emitter.emit('cmdRes', pCmd);

      pCmd = null;
    });
  }

  return {
    // 傳入 serialport 物件
    setPort: function (portIn, parserIn) {
      port = portIn; parser = parserIn;

      if (!parser.on) {
        console.error(`[ recevier ] parser on 事件註冊異常`);
        return;
      }

      parser.on('data', (data) => {
        // console.log('parser data : ', data);
        buffer.push(data[0]);
        handleData();
      });
    },
    // 刪除 serialport 物件
    destroyPort: function () {
      port = null; parser = null;
      // emitter.removeAllListeners();
    },

    // 傳入模組參數、命令集等資料
    setModule: function (moduleIn) {
      module = moduleIn;
    },

    on: function (event, callback) {
      emitter.on(event, callback);
      return emitter;
    },
    emit: function (event, arg = undefined) {
      emitter.emit(event, arg);
      return emitter;
    },
  };
};

export default receiver;
