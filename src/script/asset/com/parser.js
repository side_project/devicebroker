
/**
* serialport 資料解析器
*
* 根據 modules.js 定義的模組資料進行命令集與參數解析
*/

// import baseFcn from "../base-fcn.js";


const parser = function () {
  let module = { // 模組設定資料
    params: {}, cmds: {},
  };

  return {
    // 傳入模組參數、命令集等資料
    setModule: function (moduleIn) {
      module = moduleIn;
    },

    // 解析即將發送的命令，取得應輸入參數
    parserArrayKey: function (cmd) {
      if (Array.isArray(cmd)) {
        return extractArgKeyFromArray(cmd);
      }

      console.error(`[ parserArrayKey ] 無效的命令類型 `, cmd);
      return {};
    },

    // 比對 res key 以外的部分是否相等
    resEqual: function (res, defineRes) {
      // 如果回應定義矩陣為空，表示不需響應回應
      if (defineRes.length === 0) {
        return true;
      }

      if (res.length < defineRes.length) {
        return false;
      }

      return defineRes.every((item, i) => {
        // 為 key 參數，忽略（回傳 true）
        const key = getBracketKey(item);

        if (key) return true;

        // 判斷矩陣數值是否相等
        return res[i] === item;
      });
    },

    // 匹配 keys 與 res 數據，轉成 key-val 格式
    matchKeysResData: function (keys, res) {
      const data = {};

      Object.keys(keys).forEach((cmdKey) => {
        data[cmdKey] = keys[cmdKey].indexes.map((i) => {
          return res[i];
        });
      });

      return data;
    },

    // 根據 key index 填入 array 參數
    fillArrayParamsByKey: function (params, indexDef) {
      /*
      params: {
        arg :{
          value: "0x01 0x02"
        },
        data :{
          value: 5
        },
        data02:{
          value: 10
        }
      }

      indexDef: {
        keys: {
          arg: {
            indexes: [1, 2]
          },
          data: {
            indexes: [3]
          }
        },
        cmd: [0x01, null, null, null]
      }
      */

      const cmd = indexDef.cmd;
      if (!cmd) {
        return { status: 'err', msg: `缺少 cmd 定義` };
      }


      let status = ''; let msg = '';
      // 輪巡 indexDef 的 keys
      const err = Object.keys(indexDef.keys).some((key) => {
        const param = params[key];
        if (!param) {
          status = 'err'; msg = `輸入參數缺少 ${key}`;
          return true;
        }

        const keyIndex = indexDef.keys[key];
        if (!keyIndex) {
          status = 'err'; msg = `參數 index 定義缺少 ${key}`;
          return true;
        }

        // 判斷 value 型態
        const valType = (typeof param.value).toLowerCase();

        switch (valType) {
          case 'string': {
            const valArray = splitSpaceStrArray(param.value);
            keyIndex.indexes.forEach((indexItem, i) => {
              cmd[indexItem] = parseInt(valArray[i]);
            });
            break;
          }
          case 'number': {
            const val = param.value;

            keyIndex.indexes.forEach((indexItem, i) => {
              cmd[indexItem] = val;
            });
            break;
          }
          default: {
            status = 'err'; msg = `未知的參數型態 ${valType}`;
            return true;
          }
        }


        return false;
      });
      if (err) return { status, msg };

      return { status, msg, cmd };
    },
  };
};

export default parser;

/** 從 array 中取出參數 key。
 * @param {*} array -
 * @return {*}
 */
function extractArgKeyFromArray(array = []) {
  /*
  EX:
    Input:
      [ "0x01", "{arg}", "{arg}", "{data}" ]

    Output:
      {
        keys: {           // 所有 key
          "arg": {          // key
            indexes: [1, 2]     // key 在矩陣位置
          },
          "data": {
            indexes: [3]
          }
        },
        cmd: ["0x01", null, null, null] // 原先的 key 改為 null
      }
  */

  const data = {
    keys: {}, cmd: [],
  };

  if (array.length === 0) return data;

  data.cmd = array.map((item, i) => {
    const itemStr = String(item); // 強制轉為字串
    const key = getBracketKey(itemStr);

    if (key) {
      // 檢查是否已經生成 key 物件
      if (data.keys[key] && data.keys[key].indexes) {
        data.keys[key].indexes.push(i);
      } else {
        data.keys[key] = {
          indexes: [i],
        };
      }

      return null; // 原先的 key 改為 null
    }
    return item;
  });

  return data;
}

/** 分割空白分隔的矩陣字串
 * @param {*} arrayStr -
 * @return {*}
 */
function splitSpaceStrArray(arrayStr) {
  arrayStr = arrayStr.trim(); // 去頭去尾
  arrayStr = arrayStr.replace(/\s+/g, ' '); // 多個空白替換成一個空白

  return arrayStr.split(' ');
}

/** 取出 {key} 中 key 值
 * @param {*} str -
 * @return {*}
 */
function getBracketKey(str) {
  str = String(str);
  const result = str.match(/{(.*)}/);
  if (result !== null) {
    return result[1];
  }
  return null;
}
