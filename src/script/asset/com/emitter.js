/**
 * serialport 發射器
 *
 * 間隔指定時間發出的排程中命令
 */

import baseFcn from '../base-fcn.js';
import DataParser from './parser.js';


const emitter = function (portObj = null, options = {}) {
  let port = portObj; // serialport 物件
  let parser = null; // serialport 解析器

  let emitter = null; // EventEmitter 物件

  let enable = options.enable || true; // 開始發送
  const interval = options.interval || 150; // 命令間隔
  let intervalTimer = 0; // 紀錄時間戳記

  const timeout = options.timeout || 2500; // 超時
  let timeoutTimer = null; // 超時計時器

  let pCmd = null; // 剛發送的命令
  const queueCmds = []; // 等待發送的命令
  const dataParser = new DataParser(); // 命令解析器

  let module = { // 模組設定資料
    params: {}, cmds: {},
  };

  init();
  processCmds();

  // --- fcn ---

  /** 初始化 */
  function init() {
    if (baseFcn.isWeb()) return;

    if (!global.EventEmitter) {
      console.error(`[ emitter ] 無法載入 EventEmitter`);
      return;
    }

    const EventEmitter = global.EventEmitter;
    emitter = new EventEmitter();

    setEvent();
  }

  /** 處理命令發送細節 */
  function processCmds() {
    setInterval(() => {
      // 不須發送命令
      if (!enable || queueCmds.length === 0) {
        // console.log("[ emitter ] 不須發送命令");
        return;
      }

      // port 不可用
      if (!port || !port.isOpen) {
        // console.log("[ emitter ] port 不可用");
        return;
      }

      // 等待回應，不可發送命令
      if (timeoutTimer !== null) {
        // console.log("[ emitter ] 等待回應");
        return;
      }

      // 偵測時間差
      const ms = new Date() / 1;
      if (Math.abs(ms - intervalTimer) >= interval || intervalTimer === 0) {
        const cmd = queueCmds.shift();
        pCmd = Object.copy(cmd);
        port.write(cmd.cmd);
        // console.log("[ emitter ] cmd write : ", cmd.cmd);

        emitter.emit('sent', cmd);

        intervalTimer = ms;
      }
    }, 10);
  }

  /**  */
  function clearTimeoutTimer() {
    if (timeoutTimer === null) return;

    clearTimeout(timeoutTimer);
    timeoutTimer = null;//* /
  }

  /** 建立 emitter 事件 */
  function setEvent() {
    // COM 已成功送出資料
    emitter.on('sent', (cmd) => {
      // 開始超時計時
      timeoutTimer = setTimeout(() => {
        emitter.emit('err', {
          status: 'timeout',
          msg: `命令「${pCmd.name}」沒有回應`,
          cmd: pCmd,
        });
        clearTimeoutTimer();
      }, timeout);

      // 不需要回應，清除計時器
      if (!cmd.res || cmd.res.length === 0) {
        clearTimeoutTimer();
      }
    });


    // 回應解析成功，清除超時計時
    emitter.on('data', (data) => {
      clearTimeoutTimer();
    });
  }

  /** 增加原始格式命令
   * @param {*} cmd -
   */
  function pushQueue(cmd) {
    // console.log('[ emitter pushQueue ] cmd : ', cmd);
    queueCmds.push(cmd);
  }

  /** 清空命令駐列 */
  function clearQueue() {
    queueCmds.length = 0;
  }

  return {
    // 傳入 serialport 物件
    setPort: function (portIn, parserIn) {
      port = portIn; parser = parserIn;
    },
    // 刪除 serialport 物件
    destroyPort: function () {
      port = null;
      // eslint-disable-next-line no-unused-vars
      parser = null;
      emitter.removeAllListeners();
    },
    // 傳入模組參數、命令集等資料
    setModule: function (moduleIn) {
      module = Object.copy(moduleIn);
      // console.log(moduleIn);
    },


    // 增加排程命令，key 為命令名稱，依照 cmds.js 定義
    addCmd: function (key, params = null, extParams = null) {
      console.log(`[ emitter addCmd ] 增加 ${key} 命令`);

      /* console.log(`[ emitter addCmd ] params    : `, params);
      console.log(`[ emitter addCmd ] extParams : `, extParams);// */

      if (!module.cmds[key]) {
        // console.log(`[ emitter addCmd ] ${key} 查無此命令`);
        return {
          status: 'err', msg: `${key} 查無此命令`,
        };
      }

      const cmd = Object.copy(module.cmds[key]);

      // 檢查命令是否正常
      if (!cmd.cmd) {
        // console.log(`[ emitter addCmd ] ${key} 未定義命令數值`);
        return {
          status: 'err', msg: `${key} 未定義命令數值`,
        };
      }

      // 解析命令參數，取得對應 index
      const paramKeysIndexes = dataParser.parserArrayKey(cmd.cmd);

      // 不需要輸入參數
      if (Object.isEmpty(paramKeysIndexes.keys)) {
        this.beforeCmds(cmd.before, extParams);
        pushQueue({ key, ...cmd });
        this.thenCmds(cmd.then, extParams);

        // console.log(`[ emitter addCmd ] 命令 ${key} 新增成功（不需要輸入參數）`);
        return {
          status: 'sus', msg: `命令 ${key} 新增成功`,
        };
      }

      // 需要輸入參數，將 params 填入 cmd 中
      const res = dataParser.fillArrayParamsByKey(params, paramKeysIndexes);
      if (res.status !== '') {
        // eslint-disable-next-line max-len
        // console.log(`[ emitter addCmd ] 命令 ${key} 填入參數錯誤 ${res.status} ${res.msg}`);
        // console.log(`[ emitter addCmd ] params : `, params);
        // console.log(`[ emitter addCmd ] paramKeysIndexes : `, paramKeysIndexes);
        return {
          status: res.status, msg: res.msg,
        };
      }

      // 填入完成後，覆蓋原本命令
      cmd.cmd = res.cmd;
      this.beforeCmds(cmd.before, extParams);
      pushQueue({ key, ...cmd });
      this.thenCmds(cmd.then, extParams);

      // console.log(`[ emitter addCmd ] 命令 ${key} 新增成功`);
      return {
        status: 'sus', msg: `命令 ${key} 新增成功`,
      };
    },
    addCmds: function (cmds = []) {
      if (cmds.length === 0) return;

      cmds.forEach((cmd) => {
        this.addCmd(cmd.key, cmd.params, cmd.extParams);
      });
    },
    // 處理命令前連鎖命令
    beforeCmds: function (cmds = null, extParams = null) {
      if (!cmds || !Array.isArray(cmds) || cmds.length === 0) return;

      cmds.forEach((key) => {
        // console.log(`[ emitter beforeCmds ] 先前命令 ${key}`);
        const res = this.addCmd(key, extParams);

        if (res.status !== 'sus') {
          this.emit('err', res);
        }
      });
    },
    // 處理命令後連鎖命令
    thenCmds: function (cmds = null, extParams = null) {
      if (!cmds || !Array.isArray(cmds) || cmds.length === 0) return;

      cmds.forEach((key) => {
        // console.log(`[ emitter thenCmds ] 接續命令 ${key}`);
        const res = this.addCmd(key, extParams);

        if (res.status !== 'sus') {
          this.emit('err', res);
        }
      });
    },

    clearQueue: function () {
      clearQueue();
    },

    // 開始發送
    play: function () {
      enable = true;
    },
    // 暫停發送
    pause: function () {
      enable = false;
    },
    // 停止發送，同時清除所有排程命令
    stop: function () {
      enable = false;
      queueCmds.length = 0;
    },

    on: function (event, callback) {
      emitter.on(event, callback);
      return emitter;
    },
    emit: function (event, arg) {
      emitter.emit(event, arg);
      return emitter;
    },
  };
};

export default emitter;
