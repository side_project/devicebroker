/**
 * 參數面板，應顯示數值
 */

export default {
  'zigbee': {
    'E180-ZG120A': {
      '1.0.0': {
        allParams: {
          name: '全參數',
          description: '顯示所有參數',
          params: [ // 顯示參數，對應 params.js。{hr} 此格式可以加入排版元件，hr 是分隔線
            'deviceType', 'panId', 'netStatus',
            'coorShortAddr', 'coorMacAddr',
            'localShortAddr', 'localMacAddr', 'targetShortAddr', 'targetMacAddr',
            'channel', 'sendMode', 'outputMode',
            'power', 'bps', 'localGroupNum',
            'targetGroupNum', 'wakeCycle',
            'netOpeningHours', 'rejoinCycle', 'rejoinNum',
            'remoteHeader',

            'floor', 'room',
          ],
          setters: [ // 設定框，對應 cmds.js。{hr} 此格式可以加入排版元件，hr 是分隔線
            'setSettingMode', 'setTxMode', '{hr}',
            'getDeviceAllParams', 'getDeviceType',
            'setDeviceType', 'setSendMode', 'setTargetMac',
            'getPanId', 'setPanId', 'setRemoteHeader',

            'setRoomNum', 'getRoomNum',

            'reboot',
          ],
        },

        coordinator: {
          name: '協調器',
          description: 'Zigbee 協調器',
          params: [ // 顯示參數，對應 params.js
            'firmwareVer',
            'deviceType', 'panId', 'netStatus',
            'targetShortAddr', 'targetMacAddr',
            'channel', 'sendMode', 'outputMode',
            'power', 'localGroupNum',
            'targetGroupNum', 'netOpeningHours',
            '{hr}',

            'childCount',
            'child00DeviceType', 'child00ShortAddr', 'child00MacAddr',
            '{hr}',

            'adcPin', 'adcVal',
            '{hr}',

            'floor', 'room',
          ],
          setters: [ // 設定框，對應 cmds.js
            'setSettingMode', 'setTxMode', 'getFirmwareVer',
            '{hr}',

            'getDeviceAllParams',
            'setDeviceType', 'setSendMode', 'setTargetMac', 'setRemoteHeader',
            '{hr}',

            'getChildCount', 'getChildList', 'sendTxMode4Msg',
            '{hr}',

            'openNet', 'closeNet', 'startTouchLink',
            '{hr}',

            'remoteSetGpio', 'remoteReadAdc',
            'txMode4RemoteSetGpio', 'txMode4RemoteReadAdc',
            '{hr}',

            'setQrReaderCoor', 'getRoomNum', 'setRoomNum',
            '{hr}',

            'reboot',
          ],
        },
        router: {
          name: '路由器',
          description: 'Zigbee 路由器',
          params: [ // 顯示參數，對應 params.js
            'deviceType', 'panId', 'localShortAddr',
            'localMacAddr', 'targetShortAddr', 'targetMacAddr',
            'channel', 'sendMode', 'outputMode',
            'power', 'bps', 'localGroupNum',
            'targetGroupNum', 'netOpeningHours', 'remoteHeader',
            'rejoinCycle', 'rejoinNum',

            'floor', 'room',
          ],
          setters: [ // 設定框，對應 cmds.js
            'setSettingMode', 'setTxMode',
            '{hr}',

            'getDeviceAllParams',

            'getRoomNum', 'setRoomNum',

            'reboot',
          ],
        },
        endDevice: {
          name: '終端',
          description: 'Zigbee 終端',
          params: [ // 顯示參數，對應 params.js
            'deviceType', 'panId', 'netStatus',
            'coorShortAddr', 'coorMacAddr',
            'localShortAddr', 'localMacAddr',
            'channel', 'outputMode',
            'power', 'localGroupNum',
            'rejoinCycle', 'rejoinNum',
            'remoteHeader',

            'floor', 'room',
          ],
          setters: [ // 設定框，對應 cmds.js
            'setSettingMode', 'setTxMode', '{hr}',
            'setDeviceType',
            'getDeviceAllParams',
            'setRemoteHeader',

            'setGpio',

            'getRoomNum', 'setRoomNum',

            'reboot',
          ],
        },
        sleepEndDevice: {
          name: '休眠終端',
          description: 'Zigbee 終端',
          params: [ // 顯示參數，對應 params.js
            'deviceType', 'panId', 'netStatus',
            'coorShortAddr', 'coorMacAddr',
            'localShortAddr', 'localMacAddr',
            'channel', 'outputMode',
            'power', 'localGroupNum',
            'rejoinCycle', 'rejoinNum',

            'wakeCycle', 'remoteHeader',

            'floor', 'room',
          ],
          setters: [ // 設定框，對應 cmds.js
            'sedSetSettingMode', 'sedSetTxMode',
            '{hr}',

            'sedSetWakeCycle', 'sedSetDeviceType',
            'sedGetDeviceAllParams', 'sedSetRejoinTryNums',
            'setRemoteHeader',
            '{hr}',

            'sedSetGpio',
            '{hr}',

            'sedOpenNet', 'sedCloseNet', 'sedCreateNet', 'sedStartTouchLink',
            '{hr}',

            'setDoorCtrler', 'sedGetRoomNum', 'sedSetRoomNum',
            '{hr}',

            'sedReboot',
          ],
        },
      },
    },
  },

  'zigbee-matcher': {
    'D01': {
      '1.0.0': {
        test: {
          name: '功能測試',
          description: '',
          params: [ // 顯示參數，對應 params.js
            'p01Floor', 'p01Room',
            'p01DeviceType', 'p01TargetMacAddr',
            'p01SendMode', '{hr}',

            'p02Floor', 'p02Room',
            'p02DeviceType', 'p02LocalMacAddr',
          ],
          setters: [ // 設定框，對應 cmds.js
            'setP01SettingMode', 'getP01AllParams',
            '{hr}',
            'setP02SettingMode', 'getP02AllParams',
            '{hr}',
            'setP01TargetMac',
            'setP01DeviceType', 'setP02DeviceType',
            // 'setP01Coor', 'setP02SleepEndDevice',
            '{hr}',
            'getDeviceStatus', 'matchDevice', 'remoteSetGpio',
            'gpioTest', 'reboot',
          ],
        },
        matcher: {
          name: '配對器',
          description: '快速配對 Zigbee',
          params: [ // 顯示參數，對應 params.js
            'p01Floor', 'p01Room',
            'p01DeviceType', 'p01TargetMacAddr',
            'p01SendMode', '{hr}',

            'p02Floor', 'p02Room',
            'p02DeviceType', 'p02LocalMacAddr',
          ],
          setters: [ // 設定框，對應 cmds.js
            'getDeviceStatus', 'matchDevice', 'reboot', 'gpioTest',
          ],
        },
      },
    },
  },

  'elevator-ctrler': {
    'circuit-test': {
      '2.0.0': {
        default: {
          name: '預設',
          description: '預設面板',
          params: [ // 顯示參數，對應 params.js。{hr} 此格式可以加入排版元件，hr 是分隔線
            'ver',
            '{hr}',
            'allOutputPinsLevel', 'inputPinsLevel',
            '{hr}',
            'rtcTime',
            '{hr}',
            'romStatus',
          ],
          setters: [ // 設定框，對應 cmds.js。{hr} 此格式可以加入排版元件，hr 是分隔線
            'autoTest', 'initDevice',
            '{hr}',
            'getVer',
            '{hr}',
            'setAllOutputPinsHigh', 'setAllOutputPinsLow', 'getInputPinLevel',
            '{hr}',
            'getRtcTime',
            '{hr}',
            'getRomStatus',
          ],
        },
      },
    },
  },

  'qr-reader': {
    'zb02': {
      '1.0.0': {
        default: {
          name: '預設',
          description: '預設面板',
          params: [
            'ver', 'chipId', 'deviceId', 'fixedIp', 'serverIp', 'wsBright',
            '{hr}',
            'networkStatus', 'ip', 'wifiQuality', 'wifiSsid', 'wifiPass',
            '{hr}',
            'engrMode', 'rtcDateTime', 'mqttConnected', 'zbConnected', 'doorCtrlerBatVal', 'powerRelayStatus', 'murStatus', 'dndStatus',
            '{hr}',
            'zbType', 'zbPanId', 'zbRemoteHeader', 'zbLocalMacAddr', 'zbTargetShortMacAddr', 'zbSendMode',
          ],
          setters: [
            'getAllStatus', 'getDeviceStatus',
            '{hr}',
            'openDoor', 'reboot',
            '{hr}',
            'getNetworkStatus', 'getWifi',
            '{hr}',
            'setDeviceId', 'setServerIp', 'setFixedIp',
            '{hr}',
            'setRelayStatus', 'openDoorbell', 'playWs', 'playBeep', 'setMurDndStatus', 'getDoorCtrlerBatVal', 'setWsBright',
            '{hr}',
            'getZigbeeInfo', 'startZbSetup',

            '{hr}',
            'init',
          ],
        },
      },
    },
    'generic': {
      '3.3.0': {
        default: {
          name: '預設',
          description: '預設面板',
          params: [
            'ver', 'chipId', 'deviceId', 'deviceLevel', 'fixedIp', 'serverIp', 'wsBright', 'extEnable', 'relayLogicalNot', 'relaySecond',
            '{hr}',
            'networkStatus', 'ip', 'wifiQuality', 'wifiSsid', 'wifiPass',
            '{hr}',
            'engrMode', 'rtcDateTime', 'mqttConnected', 'zbConnected', 'doorCtrlerBatVal', 'powerRelayStatus', 'murStatus', 'dndStatus',
            '{hr}',
            'zbType', 'zbPanId', 'zbRemoteHeader', 'zbLocalMacAddr', 'zbTargetShortMacAddr', 'zbSendMode',
          ],
          setters: [
            'getAllStatus', 'getDeviceStatus',
            '{hr}',
            'openDoor', 'reboot',
            '{hr}',
            'getNetworkStatus', 'getWifi',
            '{hr}',
            'setDeviceId', 'setServerIp', 'setFixedIp', 'setDeviceLevel', 'setRelaySetting',
            '{hr}',
            'setRelayStatus', 'openDoorbell', 'playWs', 'playBeep', 'setMurDndStatus', 'getDoorCtrlerBatVal', 'setWsBright',
            '{hr}',
            'getZigbeeInfo', 'startZbSetup', 'setZbEnable',

            '{hr}',
            'init',
          ],
        },
      },
    },
  },

  'df-guard': {
    'D01': {
      '1.0.0': {
        default: {
          name: '預設',
          description: '預設面板',
          params: [
            'ver', 'networkStatus', 'ip', 'wifiQuality', 'wifiSsid',
          ],
          setters: [
            'reboot', 'getVer', 'getNetworkStatus', 'getWifiSsid',
            '{hr}',
          ],
        },
      },
    },
  },
};
