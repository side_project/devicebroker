/* 模組命令清單

cmd、res 中 {key} 值，表示為參數、回應值，名稱對應 params key 名稱

*/


export default {
  'zigbee': {
    'E180-ZG120A': {
      '1.0.0': {
        setSettingMode: {
          name: '進入設定模式',
          description: '設定模式才會響應設定命令',
          type: 'Array', format: 'HEX',
          cmd: [0x2A, 0x2D, 0x2E],
          res: [0x7A, 0x7D, 0x7E],
          then: ['getDeviceAllParams'],
        },
        setTxMode: {
          name: '進入傳輸模式',
          description: '傳輸模式會將所有 RX 接收值視為傳輸資料發送',
          type: 'Array', format: 'HEX',
          cmd: [0x2F, 0x2C, 0x2B],
          res: [0x7F, 0x7C, 0x7B],
          then: [],
        },

        getDeviceType: {
          name: '取得設備類型',
          description: '取得 Zigbee 設備類型',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x01, 0x01, 0xFF],
          res: [0xFB, 0x01, '{deviceType}'],
          then: [],
        },
        getNetStatus: {
          name: '取得網路狀態',
          description: '取得設備網路狀態',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x01, 0x02, 0xFF],
          res: [0xFB, 0x02, '{netStatus}'],
          then: [],
        },
        getPanId: {
          name: '取得網路 PANID',
          description: '取得網路 PANID',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x02, 0x03, 0xFF],
          res: [0xFB, 0x03, '{panId}', '{panId}'],
          then: [],
        },
        getLocalMac: {
          name: '取得本地 MAC 地址',
          description: '讀取模組 MAC 位置，固定無法修改',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x08, 0x06, 0xFF],
          res: [0xFB, 0x06,
            '{localMacAddr}', '{localMacAddr}', '{localMacAddr}', '{localMacAddr}',
            '{localMacAddr}', '{localMacAddr}', '{localMacAddr}', '{localMacAddr}'],
          then: [],
        },
        getCoorShortAddr: {
          name: '取得父節點短地址',
          description: '讀取協調器短地址',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x02, 0x07, 0xFF],
          res: [0xFB, 0x07, '{coorShortAddr}', '{coorShortAddr}'],
          then: [],
        },
        getCoorMacAddr: {
          name: '取得父節點 MAC 地址',
          description: '讀取協調器 64bit MAC',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x08, 0x08, 0xFF],
          res: [0xFB, 0x08,
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}'],
          then: [],
        },
        getGroupNum: {
          name: '取得網路組號',
          description: '讀取組播群組編號',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x08, 0x08, 0xFF],
          res: [0xFB, 0x08,
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}'],
          then: [],
        },
        getChannel: {
          name: '取得通訊頻道',
          description: '讀取物理頻率頻道',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x01, 0x0A, 0xFF],
          res: [0xFB, 0x0A, '{channel}'],
          then: [],
        },
        getPower: {
          name: '取得發射功率',
          description: '取得發射功率',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x01, 0x0B, 0xFF],
          res: [0xFB, 0x0B, '{power}'],
          then: [],
        },
        getBps: {
          name: '取得 BPS',
          description: '取得 TTL 通訊速度',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x01, 0x0C, 0xFF],
          res: [0xFB, 0x0C, '{bps}'],
          then: [],
        },
        getWakeCycle: {
          name: '取得休眠時間（喚醒週期）',
          description: '取得喚醒週期',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x01, 0x0D, 0xFF],
          res: [0xFB, 0x0D, '{wakeCycle}'],
          then: [],
        },
        getSendMode: {
          name: '取得資料發送方式',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x01, 0x26, 0xFF],
          res: [0xFB, 0x26, '{sendMode}'],
          then: [],
        },

        getDeviceAllParams: {
          name: '取得設備所有參數',
          description: '一次讀取模組所有設定數值',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x2F, 0xFE, 0xFF],
          res: [0xFB, 0xFE, '{deviceType}', '{netStatus}',
            '{panId}', '{panId}',
            '{localShortAddr}', '{localShortAddr}',
            '{localMacAddr}', '{localMacAddr}', '{localMacAddr}', '{localMacAddr}',
            '{localMacAddr}', '{localMacAddr}', '{localMacAddr}', '{localMacAddr}',
            '{coorShortAddr}', '{coorShortAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{localGroupNum}', '{channel}',
            '{power}', '{bps}', '{wakeCycle}',
            '{targetShortAddr}', '{targetShortAddr}', '{targetGroupNum}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{sendMode}', '{outputMode}',
            '{netOpeningHours}', '{rejoinCycle}', '{rejoinNum}',
            '{remoteHeader}', '{remoteHeader}'],
          then: ['getRoomNum'],
        },

        setDeviceType: {
          name: '設定設備類型',
          description: '設定 Zigbee 設備類型',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x01, '{deviceType}', 0xFF],
          res: [0xFA, 0x01],
          then: ['reboot'],
        },
        setPanId: {
          name: '設定 PAN ID',
          description: '協調器無法設定 PAN ID，要先切到終端設定，或是關閉網路後設定，最後記得重啟',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x02, 0x03, '{panId}', '{panId}', 0xFF],
          res: [0xFA, 0x03],
          then: [],
        },

        setTargetMac: {
          name: '設定目標 MAC',
          description: '點播通訊，直接指定終端 MAC',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x08, 0x25,
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            0xFF],
          res: [0xFA, 0x25],
          before: ['setSettingMode'],
          then: ['getDeviceAllParams'],
        },
        setSendMode: {
          name: '設定資料發送方式',
          description: '設定點播、廣播等發送模式',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x26, '{sendMode}', 0xFF],
          res: [0xFA, 0x26],
          then: ['getSendMode'],
        },

        setPower: {
          name: '設定發射功率',
          description: '設定無線發射功率',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x0B, '{power}', 0xFF],
          res: [0xFA, 0x0B],
          then: [],
        },
        setWakeCycle: {
          name: '設定休眠時間（喚醒週期）',
          description: '取得喚醒週期',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x0D, '{wakeCycle}', 0xFF],
          res: [0xFB, 0x0D],
          then: [],
        },

        setRejoinTryNums: {
          name: '終端重試連線最大次數',
          description: `0 表示斷線後不執行自動重連
                        1~254 為嘗試次數，超過次數後清除先前網路紀錄，重新掃描新網路。
                        255 表示不清除紀錄，始終重試連線先前網路`,
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x30, '{rejoinNum}', 0xFF],
          res: [0xFA, 0x30],
          then: [],
        },

        setRemoteHeader: {
          name: '設定遠端控制 Header',
          description: `預設值為 0xA8 0x8A，0x00 0x00 表示關閉遠端功能`,
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x02, 0x31, '{remoteHeader}', '{remoteHeader}', 0xFF],
          res: [0xFA, 0x31],
          then: [],
        },

        reboot: {
          name: '設備重啟',
          description: '重啟設備，套用相關設定',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x00, 0x12, 0xFF],
          res: [0xFA, 0x12],
          before: ['setSettingMode'], then: [],
        },
        restoreSetting: {
          name: '回復原廠設定',
          description: '回復所有設定值，請參考說明書',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x00, 0x13, 0xFF],
          res: [0xFA, 0x13],
          then: [],
        },


        setGpio: {
          name: 'GPIO 控制',
          description: '設定 GPIO 輸入/輸出與電位',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x03, 0x20, '{ioPin}', '{ioMode}', '{ioLevel}', 0xFF],
          res: [0xFA, 0x20],
          before: ['setSettingMode'], then: [],
        },

        remoteSetGpio: {
          name: '遠端 GPIO 控制',
          description: '設定終端的 GPIO 輸入/輸出與電位，只能使用點播，並設定目標 MAC',
          type: 'Array', format: 'HEX',
          cmd: [0xA8, 0x8A, 0xFD, 0x03, 0x20, '{ioPin}', '{ioMode}', '{ioLevel}', 0xFF],
          res: [0xA8, 0x8A, 0xFA, 0x20],
          before: ['setTxMode'], then: [],
        },

        remoteReadAdc: {
          name: '遠端讀取 ADC',
          description: '讀取終端 ADC，只能使用點播，並設定目標 MAC',
          type: 'Array', format: 'HEX',
          cmd: [0xA8, 0x8A, 0xFE, 0x03, 0x22, '{adcPin}', 0xFF],
          res: [0xFB, 0x22, '{adcPin}', '{adcVal}', '{adcVal}'],
          before: ['setTxMode'], then: [],
        },

        getChildCount: {
          name: '取得終端數量',
          description: '協調器命令，取得目前已連線之終端數量',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x01, 0x32, 0xFF],
          res: [0xFB, 0x32, '{childCount}'],
          then: [],
        },
        getChildList: {
          name: '取得所有終端明細',
          description: '依序列出所有終端設備類型、短地址、長地址',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x0E, 0x33, 0xFF],
          res: [0xFB, 0x33, 0x00, '{child00DeviceType}',
            '{child00ShortAddr}', '{child00ShortAddr}',
            '{child00MacAddr}', '{child00MacAddr}', '{child00MacAddr}', '{child00MacAddr}',
            '{child00MacAddr}', '{child00MacAddr}', '{child00MacAddr}', '{child00MacAddr}'],
          then: [],
        },

        openNet: {
          name: '開啟網路',
          description: '開啟 Zigbee 網路',
          type: 'Array', format: 'HEX',
          cmd: [0xF5, 0x01, 0x40, 0x01, 0xFF],
          res: [0xFC, 0x40, 0x00],
          then: [],
        },
        closeNet: {
          name: '關閉網路',
          description: '某些參數需先關閉 Zigbee 網路才能設定',
          type: 'Array', format: 'HEX',
          cmd: [0xF5, 0x01, 0x40, 0x02, 0xFF],
          res: [0xFC, 0x40, 0x00],
          then: [],
        },
        createNet: {
          name: '建立網路',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xF5, 0x01, 0x40, 0x03, 0xFF],
          res: [0xFC, 0x40, 0x00],
          then: [],
        },
        startTouchLink: {
          name: '開始 Touch Link',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xF5, 0x01, 0x40, 0x04, 0xFF],
          res: [0xFC, 0x40, 0x00],
          then: [],
        },

        getFirmwareVer: {
          name: '取得韌體版本',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x03, 0x34, 0xFF],
          res: [0xFB, 0x34, '{firmwareVer}', '{firmwareVer}', '{firmwareVer}'],
          then: [],
        },

        // --- 休眠終端專用 ---

        wakeup: {
          name: '喚醒命令',
          description: '發命令給休眠終端接前，需要先喚醒，才能接收命令',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          res: [],

          before: [], // 之前發送的命令
          then: [], // 之後發送的命令
        },

        sedSetSettingMode: {
          name: '進入設定模式',
          description: '設定模式才會響應設定命令',
          type: 'Array', format: 'HEX',
          cmd: [0x2A, 0x2D, 0x2E],
          res: [0x7A, 0x7D, 0x7E],

          before: ['wakeup'], then: ['getDeviceAllParams'],
        },
        sedSetTxMode: {
          name: '進入傳輸模式',
          description: '傳輸模式會將所有 RX 接收值視為傳輸資料發送',
          type: 'Array', format: 'HEX',
          cmd: [0x2F, 0x2C, 0x2B],
          res: [0x7F, 0x7C, 0x7B],
          before: ['wakeup'], then: [],
        },

        sedGetPanId: {
          name: '取得網路 PANID',
          description: '取得網路 PANID',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x02, 0x03, 0xFF],
          res: [0xFB, 0x03, '{panId}', '{panId}'],
          before: ['wakeup'], then: [],
        },

        sedSetDeviceType: {
          name: '設定設備類型',
          description: '設定 Zigbee 設備類型',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x01, '{deviceType}', 0xFF],
          res: [0xFA, 0x01],
          before: ['wakeup'], then: ['reboot'],
        },

        sedGetDeviceAllParams: {
          name: '取得設備所有參數',
          description: '一次讀取模組所有設定數值',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x2F, 0xFE, 0xFF],
          res: [0xFB, 0xFE, '{deviceType}', '{netStatus}',
            '{panId}', '{panId}',
            '{localShortAddr}', '{localShortAddr}',
            '{localMacAddr}', '{localMacAddr}', '{localMacAddr}', '{localMacAddr}',
            '{localMacAddr}', '{localMacAddr}', '{localMacAddr}', '{localMacAddr}',
            '{coorShortAddr}', '{coorShortAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{localGroupNum}', '{channel}',
            '{power}', '{bps}', '{wakeCycle}',
            '{targetShortAddr}', '{targetShortAddr}', '{targetGroupNum}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{sendMode}', '{outputMode}',
            '{netOpeningHours}', '{rejoinCycle}', '{rejoinNum}',
            '{remoteHeader}', '{remoteHeader}'],
          before: ['wakeup'], then: [],
        },

        sedSetRoomNum: {
          name: '設定房號',
          description: '設定設備對應的房號，調整 PANID，以網路區分',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x02, 0x03, '{floor}', '{room}', 0xFF],
          res: [0xFA, 0x03],
          before: ['wakeup', 'setSettingMode'],
          then: ['getRoomNum', 'getPanId'],
        },
        sedGetRoomNum: {
          name: '取得房號',
          description: '取得設備房號，對應 PANID',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x02, 0x03, 0xFF],
          res: [0xFB, 0x03, '{floor}', '{room}'],
          before: ['wakeup'], then: [],
        },

        sedSetWakeCycle: {
          name: '設定休眠時間（喚醒週期）',
          description: '設定喚醒週期',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x0D, '{wakeCycle}', 0xFF],
          res: [0xFB, 0x0D],
          before: ['wakeup'], then: [],
        },

        sedSetRejoinTryNums: {
          name: '設定重試連線最大次數',
          description: `0 表示斷線後不執行自動重連。
1~254 為嘗試次數，超過次數後清除先前網路紀錄，重新掃描新網路。
255 表示不清除紀錄，始終重試連線先前網路。`,
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x30, '{rejoinNum}', 0xFF],
          res: [0xFA, 0x30],
          before: ['wakeup'], then: [],
        },

        sedReboot: {
          name: '設備重啟',
          description: '重啟設備，套用相關設定',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x00, 0x12, 0xFF],
          res: [0xFA, 0x12],
          before: ['wakeup', 'setSettingMode'], then: [],
        },

        sedSetGpio: {
          name: 'GPIO 控制',
          description: '設定 GPIO 輸入/輸出與電位',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x03, 0x20, '{ioPin}', '{ioMode}', '{ioLevel}', 0xFF],
          res: [0xFA, 0x20],
          before: ['wakeup'], then: [],
        },

        sedOpenNet: {
          name: '開啟網路',
          description: '開啟 Zigbee 網路',
          type: 'Array', format: 'HEX',
          cmd: [0xF5, 0x01, 0x40, 0x01, 0xFF],
          res: [0xFC, 0x40, 0x00],
          before: ['wakeup'], then: [],
        },
        sedCloseNet: {
          name: '關閉網路',
          description: '某些參數需先關閉 Zigbee 網路才能設定',
          type: 'Array', format: 'HEX',
          cmd: [0xF5, 0x01, 0x40, 0x02, 0xFF],
          res: [0xFC, 0x40, 0x00],
          before: ['wakeup'], then: [],
        },
        sedCreateNet: {
          name: '建立網路',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xF5, 0x01, 0x40, 0x03, 0xFF],
          res: [0xFC, 0x40, 0x00],
          before: ['wakeup'], then: [],
        },
        sedStartTouchLink: {
          name: '開始 Touch Link',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xF5, 0x01, 0x40, 0x04, 0xFF],
          res: [0xFC, 0x40, 0x00],
          before: ['wakeup'], then: [],
        },


        // --- custom ---

        setRoomNum: {
          name: '設定房號',
          description: '設定設備對應的房號，調整 PANID，以網路區分',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x02, 0x03, '{floor}', '{room}', 0xFF],
          res: [0xFA, 0x03],
          before: ['setSettingMode'],
          then: ['closeNet', 'getRoomNum', 'getPanId', 'reboot'],
        },
        getRoomNum: {
          name: '取得房號',
          description: '取得設備房號，對應 PANID',
          type: 'Array', format: 'HEX',
          cmd: [0xFE, 0x02, 0x03, 0xFF],
          res: [0xFB, 0x03, '{floor}', '{room}'],
          then: [],
        },


        setQrReaderCoor: {
          name: '設定為 QRCode 讀取器之協調器',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0x2A, 0x2D, 0x2E],
          res: [0x7A, 0x7D, 0x7E],
          then: [
            'closeNet', 'sqrcStep02',
            'sqrcStep03', 'sqrcStep04',
            'reboot',
          ],
        },
        sqrcStep02: {
          name: '讀取器協調器步驟 02',
          description: '發射功率 20dBm',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x0B, 0x14, 0xFF],
          res: [0xFA, 0x0B],
          then: [],
        },
        sqrcStep03: {
          name: '讀取器協調器步驟 03',
          description: '設定為長地址點播模式',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x26, 0x03, 0xFF],
          res: [0xFA, 0x26],
          then: [],
        },
        sqrcStep04: {
          name: '讀取器協調器步驟 04',
          description: '設定為協調器',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x01, 0x01, 0xFF],
          res: [0xFA, 0x01],
          then: [],
        },


        setDoorCtrler: {
          name: '設定為門鎖控制器之休眠終端',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0x2A, 0x2D, 0x2E],
          res: [0x7A, 0x7D, 0x7E],
          then: [
            'closeNet',
            'sdcStep01', 'sdcStep02',
            'sdcStep03', 'sdcStep04',
            'sdcStep05', 'reboot',
          ],
          before: ['wakeup'],
        },
        sdcStep01: {
          name: '控制器終端步驟 01',
          description: '喚醒週期設為 2s',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x0D, 0x02, 0xFF],
          res: [0xFB, 0x0D],
          then: [],
        },
        sdcStep02: {
          name: '控制器終端步驟 02',
          description: '重連次數設為 1',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x30, 0x01, 0xFF],
          res: [0xFA, 0x30],
          then: [],
        },
        sdcStep03: {
          name: '控制器終端步驟 03',
          description: '重連週期設為 1 分鐘',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x29, 0x01, 0xFF],
          res: [0xFA, 0x29],
          then: [],
        },
        sdcStep04: {
          name: '控制器終端步驟 04',
          description: '設定為休眠終端',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x01, 0x01, 0x04, 0xFF],
          res: [0xFA, 0x01],
          then: [],
        },
        sdcStep05: {
          name: '控制器終端步驟 05',
          description: '設定 Remote header',
          type: 'Array', format: 'HEX',
          cmd: [0xFD, 0x02, 0x31, 0xA8, 0x8A, 0xFF],
          res: [0xFA, 0x31],
          then: [],
        },

        sendTxMode4Msg: {
          name: '發送協議點播訊息',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            '{targetShortAddr}', '{targetShortAddr}',
            '{hexMsg}', '{hexMsg}',
          ],
          res: [],
          then: [],
        },

        txMode4RemoteSetGpio: {
          name: '協議點播遠端 GPIO 控制',
          description: '設定終端的 GPIO 輸入/輸出與電位，傳輸模式須為「協議點播」',
          type: 'Array', format: 'HEX',
          cmd: [
            '{targetShortAddr}', '{targetShortAddr}',
            0xA8, 0x8A, 0xFD, 0x03, 0x20,
            '{ioPin}', '{ioMode}', '{ioLevel}', 0xFF,
          ],
          res: [0xA8, 0x8A, 0xFA, 0x20],
          before: ['setTxMode'], then: [],
        },

        txMode4RemoteReadAdc: {
          name: '協議點播遠端讀取 ADC',
          description: '讀取終端 ADC，傳輸模式須為「協議點播」',
          type: 'Array', format: 'HEX',
          cmd: [
            '{targetShortAddr}', '{targetShortAddr}',
            0xA8, 0x8A, 0xFE, 0x03, 0x22, '{adcPin}', 0xFF,
          ],
          res: [0xFB, 0x22, '{adcPin}', '{adcVal}', '{adcVal}'],
          before: ['setTxMode'], then: [],
        },
      },

      '1.0.1': {
      },
    },
  },

  'zigbee-matcher': {
    'D01': {
      '1.0.0': {
        setP01SettingMode: {
          name: 'Port 01 設備進入設定模式',
          description: '設定模式才會響應設定命令',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0x2A, 0x2D, 0x2E],
          res: [0xCC, 0x01, 0x01, 0x7A, 0x7D, 0x7E],
          then: ['getP01AllParams'],
        },
        setP01TxMode: {
          name: 'Port 01 設備進入傳輸模式',
          description: '傳輸模式會將所有 RX 接收值視為傳輸資料發送',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0x2F, 0x2C, 0x2B],
          res: [0xCC, 0x01, 0x01, 0x7F, 0x7C, 0x7B],
          then: [],
        },
        getP01AllParams: {
          name: '取得 Port 01 設備所有參數',
          description: '一次讀取模組所有設定數值',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFE, 0x2F, 0xFE, 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFB, 0xFE, '{p01DeviceType}', '{netStatus}',
            '{p01Floor}', '{p01Room}',
            '{localShortAddr}', '{localShortAddr}',
            '{localMacAddr}', '{localMacAddr}', '{localMacAddr}', '{localMacAddr}',
            '{localMacAddr}', '{localMacAddr}', '{localMacAddr}', '{localMacAddr}',
            '{coorShortAddr}', '{coorShortAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{localGroupNum}', '{channel}',
            '{power}', '{bps}', '{wakeCycle}',
            '{targetShortAddr}', '{targetShortAddr}', '{targetGroupNum}',
            '{p01TargetMacAddr}', '{p01TargetMacAddr}', '{p01TargetMacAddr}', '{p01TargetMacAddr}',
            '{p01TargetMacAddr}', '{p01TargetMacAddr}', '{p01TargetMacAddr}', '{p01TargetMacAddr}',
            '{p01SendMode}', '{outputMode}',
            '{netOpeningHours}', '{rejoinCycle}', '{rejoinNum}',
            '{remoteHeader}', '{remoteHeader}'],
          then: [],
        },

        setP02SettingMode: {
          name: 'Port 02 設備進入設定模式',
          description: '設定模式才會響應設定命令',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0x2A, 0x2D, 0x2E],
          res: [0xCC, 0x01, 0x02, 0x7A, 0x7D, 0x7E],
          before: ['wakeup'],
          then: [],
        },
        setP02TxMode: {
          name: 'Port 02 設備進入傳輸模式',
          description: '傳輸模式會將所有 RX 接收值視為傳輸資料發送',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0x2F, 0x2C, 0x2B],
          res: [0xCC, 0x01, 0x02, 0x7F, 0x7C, 0x7B],
          before: ['wakeup'],
          then: [],
        },
        getP02AllParams: {
          name: '取得 Port 02 設備所有參數',
          description: '一次讀取模組所有設定數值',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFE, 0x2F, 0xFE, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFB, 0xFE, '{p02DeviceType}', '{netStatus}',
            '{p02Floor}', '{p02Room}',
            '{localShortAddr}', '{localShortAddr}',
            '{p02LocalMacAddr}', '{p02LocalMacAddr}', '{p02LocalMacAddr}', '{p02LocalMacAddr}',
            '{p02LocalMacAddr}', '{p02LocalMacAddr}', '{p02LocalMacAddr}', '{p02LocalMacAddr}',
            '{coorShortAddr}', '{coorShortAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{localGroupNum}', '{channel}',
            '{power}', '{bps}', '{wakeCycle}',
            '{targetShortAddr}', '{targetShortAddr}', '{targetGroupNum}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{sendMode}', '{outputMode}',
            '{netOpeningHours}', '{rejoinCycle}', '{rejoinNum}',
            '{remoteHeader}', '{remoteHeader}'],
          before: ['wakeup'],
          then: [],
        },

        wakeup: {
          name: 'Port 02 喚醒命令',
          description: '發命令給休眠終端接前，需要先喚醒，才能接收命令',
          type: 'Array', format: 'HEX',
          cmd: [
            0xCC, 0x01, 0x02, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          res: [],

          before: [], // 之前發送的命令
          then: [], // 之後發送的命令
        },

        setP01DeviceType: {
          name: '設定 Port01 設備類型',
          description: '設定 Zigbee 設備類型',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFD, 0x01, 0x01, '{deviceType}', 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFA, 0x01],
          before: ['setP01SettingMode'],
          then: ['p01Reboot'],
        },
        setP02DeviceType: {
          name: '設定 Port02 設備類型',
          description: '設定 Zigbee 設備類型',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFD, 0x01, 0x01, '{deviceType}', 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFA, 0x01],
          before: ['setP02SettingMode'],
          then: ['p02Reboot'],
        },

        reboot: {
          name: '設備重啟',
          description: '重啟設備，套用相關設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0x2A, 0x2D, 0x2E],
          res: [0xCC, 0x01, 0x01, 0x7A, 0x7D, 0x7E],
          before: [],
          then: [
            'p01Reboot',
            'setP02SettingMode', 'p02Reboot',
          ],
        },
        p01Reboot: {
          name: 'Port01 設備重啟',
          description: '重啟設備，套用相關設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFD, 0x00, 0x12, 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFA, 0x12],
          before: ['wakeup'], then: [],
        },
        p02Reboot: {
          name: 'Port02 設備重啟',
          description: '重啟設備，套用相關設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFD, 0x00, 0x12, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFA, 0x12],
          before: ['wakeup'], then: [],
        },

        // --- 設備 ---

        getDeviceStatus: {
          name: '取得設備狀態',
          description: '取得 Port01、Port02 設備的狀態',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0x2A, 0x2D, 0x2E],
          res: [0xCC, 0x01, 0x01, 0x7A, 0x7D, 0x7E],
          before: [],
          then: [
            'getP01AllParams', 'setP02SettingMode', 'getP02AllParamsNoWakeup',
          ],
        },

        matchDevice: {
          name: '配對並設定設備',
          description: `配對協調器與終端點播。
          
先「取得設備狀態」後，將「Port 02 設備 MAC」複製貼上至參數「Port 01 目標 MAC 地址」。
          
設定完成後依序發送命令：
1. 「設備重啟」：讓設備載入設定。

2. 「取得設備狀態」：檢查設定是否合理：
- 2.1 兩設備「樓號」與「房號」要相同。
- 2.2 「Port 01 目標 MAC」要等於「Port 02 設備 MAC」。

3. 「遠端 GPIO 測試」：成功的話應該會發出繼電器彈跳的聲音（兩聲答）。
- 3.1 若「遠端 GPIO 測試」沒有成功，則重複嘗試「設備重啟」後「遠端
      GPIO 測試」這兩步驟。`,
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0x2A, 0x2D, 0x2E],
          res: [0xCC, 0x01, 0x01, 0x7A, 0x7D, 0x7E],
          before: [],
          then: [
            'closeP01ZbNet', 'setP01RoomNum', 'setP01TargetMac', 'getP01AllParams',
            'setP02SettingMode', 'closeP02ZbNet', 'setP02RoomNum', 'getP02AllParamsNoWakeup',

            'setP01Coor', 'setP02SleepEndDevice',
          ],
        },
        closeP01ZbNet: {
          name: '關閉 Port01 網路',
          description: '某些參數需先關閉 Zigbee 網路才能設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xF5, 0x01, 0x40, 0x02, 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFC, 0x40, 0x00],
          then: [],
        },
        closeP02ZbNet: {
          name: '關閉 Port02 網路',
          description: '某些參數需先關閉 Zigbee 網路才能設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xF5, 0x01, 0x40, 0x02, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFC, 0x40, 0x00],
          before: [],
          then: [],
        },
        setP01RoomNum: {
          name: '設定 Port01 房號',
          description: '設定設備對應的房號，調整 PANID，以網路區分',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFD, 0x02, 0x03, '{floor}', '{room}', 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFA, 0x03],
          before: [],
          then: [],
        },
        setP02RoomNum: {
          name: '設定 Port02 房號',
          description: '設定設備對應的房號，調整 PANID，以網路區分',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFD, 0x02, 0x03, '{floor}', '{room}', 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFA, 0x03],
          before: [],
          then: [],
        },
        getP02AllParamsNoWakeup: {
          name: '取得 Port 02 設備所有參數',
          description: '一次讀取模組所有設定數值',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFE, 0x2F, 0xFE, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFB, 0xFE, '{p02DeviceType}', '{netStatus}',
            '{p02Floor}', '{p02Room}',
            '{localShortAddr}', '{localShortAddr}',
            '{p02LocalMacAddr}', '{p02LocalMacAddr}', '{p02LocalMacAddr}', '{p02LocalMacAddr}',
            '{p02LocalMacAddr}', '{p02LocalMacAddr}', '{p02LocalMacAddr}', '{p02LocalMacAddr}',
            '{coorShortAddr}', '{coorShortAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}', '{coorMacAddr}',
            '{localGroupNum}', '{channel}',
            '{power}', '{bps}', '{wakeCycle}',
            '{targetShortAddr}', '{targetShortAddr}', '{targetGroupNum}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}', '{targetMacAddr}',
            '{sendMode}', '{outputMode}',
            '{netOpeningHours}', '{rejoinCycle}', '{rejoinNum}',
            '{remoteHeader}', '{remoteHeader}'],
          before: [],
          then: [],
        },
        setP01TargetMac: {
          name: '設定 Port01 目標 MAC',
          description: '點播通訊，直接指定終端 MAC',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFD, 0x08, 0x25,
            '{p01TargetMacAddr}', '{p01TargetMacAddr}', '{p01TargetMacAddr}', '{p01TargetMacAddr}',
            '{p01TargetMacAddr}', '{p01TargetMacAddr}', '{p01TargetMacAddr}', '{p01TargetMacAddr}',
            0xFF],
          res: [0xCC, 0x01, 0x01, 0xFA, 0x25],
          before: ['setP01SettingMode'],
          then: [],
        },


        remoteSetGpio: {
          name: '遠端 GPIO 控制',
          description: '設定終端的 GPIO 輸入/輸出與電位，只能使用點播',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xA8, 0x8A, 0xFD, 0x03, 0x20, '{ioPin}', '{ioMode}', '{ioLevel}', 0xFF],
          res: [0xCC, 0x01, 0x01, 0xA8, 0x8A, 0xFA, 0x20],
          before: ['setP01TxMode'], then: [],
        },


        gpioTest: {
          name: '遠端 GPIO 測試',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0x2F, 0x2C, 0x2B],
          res: [0xCC, 0x01, 0x01, 0x7F, 0x7C, 0x7B],
          before: [],
          then: [
            'setP02TxMode', 'remoteSetGpioH', 'remoteSetGpioH', 'remoteSetGpioL', 'remoteSetGpioL',
          ],
        },
        remoteSetGpioH: {
          name: '遠端 GPIO 設為 HIGH',
          description: '設定終端的 GPIO 輸入/輸出與電位，只能使用點播',
          type: 'Array', format: 'HEX',
          cmd: [
            0xCC, 0x01, 0x01, 0xA8, 0x8A, 0xFD,
            0x03, 0x20, 0x00, 0x00, 0x01, 0xFF,
          ],
          res: [0xCC, 0x01, 0x01, 0xA8, 0x8A, 0xFA, 0x20],
          before: [], then: [],
        },
        remoteSetGpioL: {
          name: '遠端 GPIO 設為 LOW',
          description: '設定終端的 GPIO 輸入/輸出與電位，只能使用點播',
          type: 'Array', format: 'HEX',
          cmd: [
            0xCC, 0x01, 0x01, 0xA8, 0x8A, 0xFD,
            0x03, 0x20, 0x00, 0x00, 0x00, 0xFF,
          ],
          res: [0xCC, 0x01, 0x01, 0xA8, 0x8A, 0xFA, 0x20],
          before: [], then: [],
        },


        setP01Coor: {
          name: '設定 Port01 設備為協調器',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0x2A, 0x2D, 0x2E],
          res: [0xCC, 0x01, 0x01, 0x7A, 0x7D, 0x7E],
          before: ['setP01SettingMode'],
          then: [
            'sqrcStep01', 'sqrcStep02', 'sqrcStep03',
            'sqrcStep04', 'sqrcStep05',
          ],
        },
        sqrcStep01: {
          name: '步驟 01',
          description: '某些參數需先關閉 Zigbee 網路才能設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xF5, 0x01, 0x40, 0x02, 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFC, 0x40, 0x00],
          then: [],
        },
        sqrcStep02: {
          name: '步驟 02',
          description: '發射功率 20dBm',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFD, 0x01, 0x0B, 0x14, 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFA, 0x0B],
          then: [],
        },
        sqrcStep03: {
          name: '步驟 03',
          description: '設定為長地址點播模式',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFD, 0x01, 0x26, 0x03, 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFA, 0x26],
          then: [],
        },
        sqrcStep04: {
          name: '步驟 04',
          description: '設定為協調器',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFD, 0x01, 0x01, 0x01, 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFA, 0x01],
          then: [],
        },
        sqrcStep05: {
          name: '步驟 05',
          description: '重啟設備，套用相關設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x01, 0xFD, 0x00, 0x12, 0xFF],
          res: [0xCC, 0x01, 0x01, 0xFA, 0x12],
          before: ['getP01AllParams'],
          then: [],
        },


        setP02SleepEndDevice: {
          name: '設定 Port02 設備為休眠終端',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0x2A, 0x2D, 0x2E],
          res: [0xCC, 0x01, 0x02, 0x7A, 0x7D, 0x7E],
          before: ['wakeup', 'setP02SettingMode'],
          then: [
            'sdcStep00', 'sdcStep01', 'sdcStep02',
            'sdcStep03', 'sdcStep04', 'sdcStep05',
          ],
        },
        sdcStep00: {
          name: '步驟 00',
          description: '某些參數需先關閉 Zigbee 網路才能設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xF5, 0x01, 0x40, 0x02, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFC, 0x40, 0x00],
          then: [],
        },
        sdcStep01: {
          name: '步驟 01',
          description: '喚醒週期設為 2s',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFD, 0x01, 0x0D, 0x02, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFB, 0x0D],
          then: [],
        },
        sdcStep02: {
          name: '步驟 02',
          description: '重連次數設為 255',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFD, 0x01, 0x30, 0xFF, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFA, 0x30],
          then: [],
        },
        sdcStep03: {
          name: '步驟 03',
          description: '重連週期設為 1 分鐘',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFD, 0x01, 0x29, 0x01, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFA, 0x29],
          then: [],
        },
        sdcStep04: {
          name: '步驟 04',
          description: '設定為休眠終端',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFD, 0x01, 0x01, 0x04, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFA, 0x01],
          then: [],
        },
        sdcStep05: {
          name: '步驟 05',
          description: '重啟設備，套用相關設定',
          type: 'Array', format: 'HEX',
          cmd: [0xCC, 0x01, 0x02, 0xFD, 0x00, 0x12, 0xFF],
          res: [0xCC, 0x01, 0x02, 0xFA, 0x12],
          before: ['getP02AllParams'],
          then: [],
        },

        // --- 共用變數命令 ---


      },
    },
  },

  'elevator-ctrler': {
    'circuit-test': {
      '2.0.0': {
        initDevice: {
          name: '初始化設備',
          description: '初始化所有參數，才能開始後續測試',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x02],
          res: [0xFB, 0x00, 0x02],
          then: ['getRtcTime', 'getRomStatus'],
        },

        getVer: {
          name: '取得韌體版本',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x01],
          res: [0xFB, 0x00, 0x01, 0x03, '{ver}', '{ver}', '{ver}'],
          then: [],
        },

        setAllOutputPinsHigh: {
          name: '設所有 MCP Output Pin HIGH',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x01, 0x01, 0x01, 0x01],
          res: [0xFB, 0x01, 0x01, 0x01, '{allOutputPinsLevel}'],
          then: [],
        },
        setAllOutputPinsLow: {
          name: '設所有 MCP Output Pin LOW',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x01, 0x01, 0x01, 0x00],
          res: [0xFB, 0x01, 0x01, 0x01, '{allOutputPinsLevel}'],
          then: [],
        },

        getInputPinLevel: {
          name: '取得輸入腳位電壓',
          description: 'MCP03 之 A6、B0、B1、B2、B3 為輸入腳位。',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x01, 0x02],
          res: [0xFB, 0x01, 0x02, 0x01, '{inputPinsLevel}'],
          then: [],
        },

        getRtcTime: {
          name: '取得 RTC 時間',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x01, 0x03],
          res: [0xFB, 0x01, 0x03, 0x0E, '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}', '{rtcTime}'],
          then: [],
        },

        getRomStatus: {
          name: '取得 ROM 狀態',
          description: '測試外部 EEPROM 功能',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x01, 0x04],
          res: [0xFB, 0x01, 0x04, 0x01, '{romStatus}'],
          then: [],
        },

        autoTest: {
          name: '自動初始化、動作繼電器並讀取所有狀態',
          description: '自動測試所有功能',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x01, 0x01, 0x01, 0x01],
          res: [0xFB, 0x01, 0x01, 0x01, '{allOutputPinsLevel}'],
          then: [
            'initDevice', 'getVer', 'getInputPinLevel',
            'getRtcTime', 'getRomStatus', 'setAllOutputPinsLow',
          ],
        },
      },
    },
  },

  'qr-reader': {
    'zb02': {
      '1.0.0': {
        reboot: {
          name: '重啟',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x01, 0x00],
          res: [],
          then: [],
        },

        init: {
          name: '初始化',
          description: '將所有設備參數設定為預設值',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x02, 0x00],
          res: [],
          then: [],
        },

        getAllStatus: {
          name: '取得各類系統資訊',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x03, 0x00],
          res: [
            0xFB, 0x00, 0x03, 11,
            '{ver}', '{ver}', '{ver}',
            '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}',
          ],
          then: [
            'getDeviceId', 'getNetworkStatus',
            'getWifi', 'getZigbeeInfo', 'getServerIp',
            'getFixedIp', 'getDeviceStatus',
          ],
        },

        getSystemInfo: {
          name: '取得系統資訊',
          description: '韌體版本、晶片 ID 等等',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x03, 0x00],
          res: [
            0xFB, 0x00, 0x03, 11,
            '{ver}', '{ver}', '{ver}',
            '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}',
          ],
          then: [],
        },

        getNetworkStatus: {
          name: '取得網路狀態',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x04, 0x00],
          res: [
            0xFB, 0x00, 0x04, 0x05,
            '{networkStatus}',
            '{ip}', '{ip}', '{ip}', '{ip}',
          ],
          then: [],
        },

        getWifi: {
          name: `取得 Wi-Fi 資訊`,
          description: `32 位 SSID 與 64 位密碼`,
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x05, 0x00],
          res: [0xFB, 0x00, 0x05, 97,
            '{wifiQuality}',

            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',

            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
          ],
          then: [],
        },

        getDeviceId: {
          name: '取得設備資訊',
          description: '設備 ID',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x06, 0x00],
          res: [
            0xFB, 0x00, 0x06, 0x02,
            '{deviceId}', '{deviceId}',
          ],
          then: [],
        },

        setDeviceId: {
          name: '設定設備 ID',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x06, 0x02, '{deviceId}', '{deviceId}'],
          res: [
            0xFB, 0x00, 0x06, 0x06,
            '{deviceId}', '{deviceId}',
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        getFixedIp: {
          name: '取得固定 IP',
          description: '0 0 0 0 表示使用 DHCP',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x00, 0x07, 0x00,
          ],
          res: [
            0xFB, 0x00, 0x07, 0x04,
            '{fixedIp}', '{fixedIp}', '{fixedIp}', '{fixedIp}',
          ],
          then: [],
        },
        setFixedIp: {
          name: '設定固定 IP',
          description: '0 0 0 0 表示使用 DHCP',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x00, 0x07, 0x04,
            '{fixedIp}', '{fixedIp}', '{fixedIp}', '{fixedIp}',
          ],
          res: [
            0xFB, 0x00, 0x07, 0x04,
            '{fixedIp}', '{fixedIp}', '{fixedIp}', '{fixedIp}',
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        getZigbeeInfo: {
          name: '取得 Zigbee 模組重要參數',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x01, 0x01, 0x00],
          res: [
            0xFB, 0x01, 0x01, 14,
            '{zbType}',
            '{zbPanId}', '{zbPanId}',
            '{zbLocalMacAddr}', '{zbLocalMacAddr}', '{zbLocalMacAddr}', '{zbLocalMacAddr}',
            '{zbLocalMacAddr}', '{zbLocalMacAddr}', '{zbLocalMacAddr}', '{zbLocalMacAddr}',
            '{zbTargetShortMacAddr}', '{zbTargetShortMacAddr}',
            '{zbSendMode}',
            '{zbRemoteHeader}', '{zbRemoteHeader}',
          ],
          then: [],
        },

        startZbSetup: {
          name: '自動設定 Zigbee 模組',
          description: '根據目前設定，自動設定模組參數',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x01, 0x02, 0x00,
          ],
          res: [
            0xFB, 0x01, 0x02, 0x00,
          ],
          then: ['getZigbeeInfo'],
        },

        openDoor: {
          name: '開門',
          description: '下令門鎖控制器開門',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x01, 0x00,
          ],
          res: [
            0xFB, 0x02, 0x01, 0x00,
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        getServerIp: {
          name: '取得伺服器 IP',
          description: '取得已儲存之區域伺服器 IP',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x03, 0x01, 0x00,
          ],
          res: [
            0xFB, 0x03, 0x01, 0x04,
            '{serverIp}', '{serverIp}', '{serverIp}', '{serverIp}',
          ],
          then: [],
        },
        setServerIp: {
          name: '設定伺服器 IP',
          description: '設定區域伺服器 IP',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x03, 0x01, 0x04,
            '{serverIp}', '{serverIp}', '{serverIp}', '{serverIp}',
          ],
          res: [
            0xFB, 0x03, 0x01, 0x04,
            '{serverIp}', '{serverIp}', '{serverIp}', '{serverIp}',
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        getDeviceStatus: {
          name: '取得設備狀態',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x03, 0x00,
          ],
          res: [
            0xFB, 0x02, 0x03, 0xFF,
            '{engrMode}',

            '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}',
            '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}',
            '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}',
            '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}',

            '{mqttConnected}', '{zbConnected}',

            '{powerRelayStatus}', '{murStatus}', '{dndStatus}',
          ],
          then: [],
        },

        enableEngrMode: {
          name: '開啟工程模式',
          description: '可進行參數設定',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x02, 0x01, 1,
          ],
          res: [
            0xFB, 0x02, 0x02, 0x01, '{engrMode}',
          ],
          then: [],
        },

        getWsBright: {
          name: '取得燈條亮度',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x04, 0x01, 0x00,
          ],
          res: [
            0xFB, 0x04, 0x01, 0x01, '{wsBright}',
          ],
          then: [],
        },

        setWsBright: {
          name: '設定燈條亮度',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x04, 0x01, 0x01, '{wsBright}',
          ],
          res: [
            0xFB, 0x04, 0x01, 0x01, '{wsBright}',
          ],
          then: [],
        },

        playWs: {
          name: '播放燈條',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x04, 0x02, 0x01, '{wsPlayType}',
          ],
          res: [
            0xFB, 0x04, 0x02, 0x00,
          ],
          then: [],
        },

        getDoorCtrlerBatVal: {
          name: '取得控制器電池電量',
          description: '低於 5.1V 表示低電量',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x05, 0x01, 0x00,
          ],
          res: [
            0xFB, 0x05, 0x01, 0x02, '{doorCtrlerBatVal}', '{doorCtrlerBatVal}',
          ],
          then: [],
        },

        playBeep: {
          name: '播放指定蜂鳴器曲目',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x04, 0x03, 0x01, '{beepPlayType}',
          ],
          res: [
            0xFB, 0x04, 0x03, 0x00,
          ],
          then: [],
        },

        setRelayStatus: {
          name: '設定節電器繼電器狀態',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x04, 0x01, '{powerRelayStatus}',
          ],
          res: [
            0xFB, 0x02, 0x04, 0x01, '{powerRelayStatus}',
          ],
          then: [],
        },

        getMurDndStatus: {
          name: '取得掃房、勿擾燈狀態',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x05, 0x00,
          ],
          res: [
            0xFB, 0x02, 0x05, 0x02, '{murStatus}', '{dndStatus}',
          ],
          then: [],
        },

        setMurDndStatus: {
          name: '設定掃房、勿擾燈狀態',
          description: '兩種燈號狀態互斥，一次只會亮一種狀態',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x05, 0x02, '{roomServiceLedType}', '{ledStatus}',
          ],
          res: [
            0xFB, 0x02, 0x05, 0x02, '{murStatus}', '{dndStatus}',
          ],
          then: [],
        },

        openDoorbell: {
          name: '響門鈴',
          description: '開啟 MOSFET 輸出',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x06, 0x02, 10,
          ],
          res: [
            0xFB, 0x02, 0x06, 0x00,
          ],
          then: [],
        },
      },
    },
    'generic': {
      '3.3.0': {
        reboot: {
          name: '重啟',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x01, 0x00],
          res: [],
          then: [],
        },

        init: {
          name: '初始化',
          description: '將所有設備參數設定為預設值',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x02, 0x00],
          res: [],
          then: [],
        },

        getAllStatus: {
          name: '取得各類系統資訊',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x03, 0x00],
          res: [
            0xFB, 0x00, 0x03, 11,
            '{ver}', '{ver}', '{ver}',
            '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}',
          ],
          then: [
            'getDeviceId', 'getNetworkStatus',
            'getWifi', 'getZigbeeInfo', 'getServerIp',
            'getFixedIp', 'getDeviceStatus',
          ],
        },

        getSystemInfo: {
          name: '取得系統資訊',
          description: '韌體版本、晶片 ID 等等',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x03, 0x00],
          res: [
            0xFB, 0x00, 0x03, 11,
            '{ver}', '{ver}', '{ver}',
            '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}', '{chipId}',
          ],
          then: [],
        },

        getNetworkStatus: {
          name: '取得網路狀態',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x04, 0x00],
          res: [
            0xFB, 0x00, 0x04, 0x05,
            '{networkStatus}',
            '{ip}', '{ip}', '{ip}', '{ip}',
          ],
          then: [],
        },

        getWifi: {
          name: `取得 Wi-Fi 資訊`,
          description: `32 位 SSID 與 64 位密碼`,
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x05, 0x00],
          res: [0xFB, 0x00, 0x05, 97,
            '{wifiQuality}',

            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',

            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
            '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}', '{wifiPass}',
          ],
          then: [],
        },

        getDeviceId: {
          name: '取得設備資訊',
          description: '設備 ID',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x06, 0x00],
          res: [
            0xFB, 0x00, 0x06, 0x02,
            '{deviceId}', '{deviceId}',
          ],
          then: [],
        },

        setDeviceId: {
          name: '設定設備 ID',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x06, 0x02, '{deviceId}', '{deviceId}'],
          res: [
            0xFB, 0x00, 0x06, 0x06,
            '{deviceId}', '{deviceId}',
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        getFixedIp: {
          name: '取得固定 IP',
          description: '0 0 0 0 表示使用 DHCP',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x00, 0x07, 0x00,
          ],
          res: [
            0xFB, 0x00, 0x07, 0x04,
            '{fixedIp}', '{fixedIp}', '{fixedIp}', '{fixedIp}',
          ],
          then: [],
        },
        setFixedIp: {
          name: '設定固定 IP',
          description: '0 0 0 0 表示使用 DHCP',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x00, 0x07, 0x04,
            '{fixedIp}', '{fixedIp}', '{fixedIp}', '{fixedIp}',
          ],
          res: [
            0xFB, 0x00, 0x07, 0x04,
            '{fixedIp}', '{fixedIp}', '{fixedIp}', '{fixedIp}',
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        getZigbeeInfo: {
          name: '取得 Zigbee 模組重要參數',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x01, 0x01, 0x00],
          res: [
            0xFB, 0x01, 0x01, 14,
            '{zbType}',
            '{zbPanId}', '{zbPanId}',
            '{zbLocalMacAddr}', '{zbLocalMacAddr}', '{zbLocalMacAddr}', '{zbLocalMacAddr}',
            '{zbLocalMacAddr}', '{zbLocalMacAddr}', '{zbLocalMacAddr}', '{zbLocalMacAddr}',
            '{zbTargetShortMacAddr}', '{zbTargetShortMacAddr}',
            '{zbSendMode}',
            '{zbRemoteHeader}', '{zbRemoteHeader}',
          ],
          then: [],
        },

        startZbSetup: {
          name: '自動設定 Zigbee 模組',
          description: '根據目前設定，自動設定模組參數',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x01, 0x02, 0x00,
          ],
          res: [
            0xFB, 0x01, 0x02, 0x00,
          ],
          then: ['getZigbeeInfo'],
        },

        openDoor: {
          name: '開門',
          description: '下令門鎖控制器開門',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x01, 0x00,
          ],
          res: [
            0xFB, 0x02, 0x01, 0x00,
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        getServerIp: {
          name: '取得伺服器 IP',
          description: '取得已儲存之區域伺服器 IP',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x03, 0x01, 0x00,
          ],
          res: [
            0xFB, 0x03, 0x01, 0x04,
            '{serverIp}', '{serverIp}', '{serverIp}', '{serverIp}',
          ],
          then: [],
        },
        setServerIp: {
          name: '設定伺服器 IP',
          description: '設定區域伺服器 IP',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x03, 0x01, 0x04,
            '{serverIp}', '{serverIp}', '{serverIp}', '{serverIp}',
          ],
          res: [
            0xFB, 0x03, 0x01, 0x04,
            '{serverIp}', '{serverIp}', '{serverIp}', '{serverIp}',
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        getDeviceStatus: {
          name: '取得設備狀態',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x03, 0x00,
          ],
          res: [
            0xFB, 0x02, 0x03, 0xFF,
            '{engrMode}',

            '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}',
            '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}',
            '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}',
            '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}', '{rtcDateTime}',

            '{mqttConnected}', '{zbConnected}',

            '{powerRelayStatus}', '{murStatus}', '{dndStatus}', '{extEnable}',

            '{deviceLevel}', '{relayLogicalNot}', '{relaySecond}',
          ],
          then: [],
        },

        enableEngrMode: {
          name: '開啟工程模式',
          description: '可進行參數設定',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x02, 0x01, 1,
          ],
          res: [
            0xFB, 0x02, 0x02, 0x01, '{engrMode}',
          ],
          then: [],
        },

        getWsBright: {
          name: '取得燈條亮度',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x04, 0x01, 0x00,
          ],
          res: [
            0xFB, 0x04, 0x01, 0x01, '{wsBright}',
          ],
          then: [],
        },

        setWsBright: {
          name: '設定燈條亮度',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x04, 0x01, 0x01, '{wsBright}',
          ],
          res: [
            0xFB, 0x04, 0x01, 0x01, '{wsBright}',
          ],
          then: [],
        },

        playWs: {
          name: '播放燈條',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x04, 0x02, 0x01, '{wsPlayType}',
          ],
          res: [
            0xFB, 0x04, 0x02, 0x00,
          ],
          then: [],
        },

        getDoorCtrlerBatVal: {
          name: '取得控制器電池電量',
          description: '低於 5.1V 表示低電量',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x05, 0x01, 0x00,
          ],
          res: [
            0xFB, 0x05, 0x01, 0x02, '{doorCtrlerBatVal}', '{doorCtrlerBatVal}',
          ],
          then: [],
        },

        playBeep: {
          name: '播放指定蜂鳴器曲目',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x04, 0x03, 0x01, '{beepPlayType}',
          ],
          res: [
            0xFB, 0x04, 0x03, 0x00,
          ],
          then: [],
        },

        setRelayStatus: {
          name: '設定節電器繼電器動作',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x04, 0x01, '{powerRelayStatus}',
          ],
          res: [
            0xFB, 0x02, 0x04, 0x01, '{powerRelayStatus}',
          ],
          then: [],
        },

        getMurDndStatus: {
          name: '取得掃房、勿擾燈狀態',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x05, 0x00,
          ],
          res: [
            0xFB, 0x02, 0x05, 0x02, '{murStatus}', '{dndStatus}',
          ],
          then: [],
        },

        setMurDndStatus: {
          name: '設定掃房、勿擾燈狀態',
          description: '兩種燈號狀態互斥，一次只會亮一種狀態',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x05, 0x02, '{roomServiceLedType}', '{ledStatus}',
          ],
          res: [
            0xFB, 0x02, 0x05, 0x02, '{murStatus}', '{dndStatus}',
          ],
          then: [],
        },

        openDoorbell: {
          name: '響門鈴',
          description: '開啟 MOSFET 輸出',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x06, 0x02, 10,
          ],
          res: [
            0xFB, 0x02, 0x06, 0x00,
          ],
          then: [],
        },

        setZbEnable: {
          name: '開關 ZB 功能',
          description: '關閉後會忽略 ZB 離線狀態、停用 ZB 偵測等等功能',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x01, 0x03, 0x01, '{enableValue}',
          ],
          res: [
            0xFB, 0x01, 0x03, 0x00,
          ],
          then: [],
        },

        setDeviceLevel: {
          name: '設定設備等級',
          description: '房間設備必須為 0x22（34），公共空間為 0x10（16）',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x07, 0x01, '{deviceLevel}',
          ],
          res: [
            0xFB, 0x02, 0x07, 0x01, '{deviceLevel}',
          ],
          before: ['enableEngrMode'],
          then: [],
        },

        setRelaySetting: {
          name: '設定 Relay 功能',
          description: '動作邏輯與動作時間',
          type: 'Array', format: 'HEX',
          cmd: [
            0xFA, 0x02, 0x08, 0x02, '{relayLogicalNot}', '{relaySecond}',
          ],
          res: [
            0xFB, 0x02, 0x08, 0x02, '{relayLogicalNot}', '{relaySecond}',
          ],
          then: [],
        },
      },
    },
  },

  'df-guard': {
    'D01': {
      '1.0.0': {
        reboot: {
          name: '重啟',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x01],
          res: [],
          then: [],
        },

        getVer: {
          name: '取得韌體版本',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x02],
          res: [0xFB, 0x00, 0x02, 0x03, '{ver}', '{ver}', '{ver}'],
          then: [],
        },

        getNetworkStatus: {
          name: '網路狀態',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x03],
          res: [0xFB, 0x00, 0x03, 0x06, '{networkStatus}', '{wifiQuality}', '{ip}', '{ip}', '{ip}', '{ip}'],
          then: [],
        },

        getWifiSsid: {
          name: '取得 Wi-Fi SSID',
          description: '',
          type: 'Array', format: 'HEX',
          cmd: [0xFA, 0x00, 0x04],
          res: [0xFB, 0x00, 0x04, 32,
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
            '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}', '{wifiSsid}',
          ],
          then: [],
        },
      },
    },
  },
};
