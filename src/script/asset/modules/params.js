
/*
模組參數清單。

模組種類


名稱、說明、數值型態、範圍、種類
*/

export default {
  'zigbee': {
    'E180-ZG120A': {
      '1.0.0': {
        deviceType: {
          name: '設備類型',
          description: 'Zigbee 設備種類。更改類型後需重啟模組，設定才能生效',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '協調器' },
            { val: 2, description: '路由器' },
            { val: 3, description: '終端（預設）' },
            { val: 4, description: '休眠終端' },
          ],
        },
        panId: {
          name: 'PANID',
          description: 'Zigbee 網路識別符，同網路內所有設備 PANID 需相同',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        netStatus: {
          name: '網路狀態',
          description: '目前設備網路狀態',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '無網路' },
            { val: 1, description: '正在加入網路' },
            { val: 2, description: '成功加入網路' },
            { val: 3, description: '有網路存在但失去父節點' },
            { val: 4, description: '正在離開網路' },
          ],
        },

        coorShortAddr: {
          name: '父節點網路短地址',
          description: 'Zigbee 網路中協調器短地址',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        coorMacAddr: {
          name: '父節點網路 MAC 地址',
          description: 'Zigbee 網路中協調器 MAC 地址',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },

        localShortAddr: {
          name: '本地網路短地址',
          description: 'Zigbee 設備於網路之地址，由協調器分配，協調器本身為 0x0000',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        localMacAddr: {
          name: '本地 MAC 地址',
          description: '模組 64 bit MAC 地址，不可修改',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        targetShortAddr: {
          name: '目標網路短地址',
          description: '通訊目標短地址',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        targetMacAddr: {
          name: '目標 MAC 地址',
          description: '定點通訊模式使用，目標模組 64 bit MAC 地址',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        child00DeviceType: {
          name: '序號 00 子終端設備類型',
          description: 'Zigbee 設備種類',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '協調器' },
            { val: 2, description: '路由器' },
            { val: 3, description: '終端（預設）' },
            { val: 4, description: '休眠終端' },
          ],
        },
        child00ShortAddr: {
          name: '序號 00 子終端網路短地址',
          description: '',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        child00MacAddr: {
          name: '序號 00 子終端 MAC 地址',
          description: '目前已連線之終端',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        channel: {
          name: '頻道',
          description: 'Zigbee 物理頻率頻道',
          type: 'Number', format: 'HEX',
          max: 0x1A, min: 0x0B,
        },
        sendMode: {
          name: '發送模式',
          description: '模組之間訊息發送模式',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '廣播（預設）' },
            { val: 1, description: '組播' },
            { val: 2, description: '短地址點播' },
            { val: 3, description: '長地址點播' },
            { val: 4, description: '協議點播' },
            { val: 5, description: '協議組播' },
          ],
        },
        outputMode: {
          name: '訊息輸出模式',
          description: '模組輸出接收數據模式',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '透傳' },
            { val: 1, description: '數據+短地址' },
            { val: 2, description: '數據+長地址' },
            { val: 3, description: '數據+RSSI' },
            { val: 4, description: '數據+短地址+RSSI' },
            { val: 5, description: '數據+長地址+RSSI' },
          ],
        },
        power: {
          name: '發射功率',
          description: '模組無線通訊發射功率',
          type: 'Number', format: 'DEC',
          max: 20, min: 0,
        },
        bps: {
          name: 'BPS',
          description: 'TX/RX TTL 通訊速度',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0x01, description: '4800' },
            { val: 0x02, description: '9600' },
            { val: 0x03, description: '14400' },
            { val: 0x04, description: '19200' },
            { val: 0x05, description: '38400' },
            { val: 0x06, description: '50000' },
            { val: 0x07, description: '57600' },
            { val: 0x08, description: '76800' },
            { val: 0x09, description: '115200 (預設)' },
            { val: 0x0A, description: '128000' },
            { val: 0x0B, description: '230400' },
            { val: 0x0C, description: '256000' },
            { val: 0x0D, description: '460800' },
          ],
        },

        localGroupNum: {
          name: '本地網路組號',
          description: '設備在此網路組號，組播通訊使用。0 表示沒有分組，為廣播',
          type: 'Number', format: 'HEX',
          max: 0xFF, min: 0x00,
        },
        targetGroupNum: {
          name: '目標網路組號',
          description: '組播通訊之網路組號，組播通訊使用。0 表示沒有分組，為廣播',
          type: 'Number', format: 'HEX',
          max: 0xFF, min: 0x00,
        },

        wakeCycle: {
          name: '喚醒週期',
          description: `休眠終端專用。週期越大，平均功耗越低，但是系統響應越慢。
1~60 表示 1~60 秒，61~255 表示 60+(61-60)*10~60+(255-60)*10 秒。`,
          type: 'Number', format: 'DEC',
          max: 255, min: 1,
        },

        netOpeningHours: {
          name: '網路開放時間',
          description: '開放時間任何設備可自由加入，1~254*10 秒，255 表示永久開放',
          type: 'Number', format: 'DEC',
          max: 255, min: 1,
        },
        rejoinCycle: {
          name: '父節點重連週期',
          description: `當父節點斷線後，重新嘗試連線週期。單位為分鐘`,
          type: 'Number', format: 'DEC',
          max: 255, min: 1,
        },
        rejoinNum: {
          name: '父節點重連次數',
          description: `0 表示斷線後不執行自動重連。
1~254 為嘗試次數，超過次數後清除先前網路紀錄，重新掃描新網路。
255 表示不清除紀錄，始終重試連線先前網路。`,
          type: 'Number', format: 'DEC',
          max: 255, min: 1,
        },
        childCount: {
          name: '節點數量',
          description: `協調器專用，目前已連線之終端數量`,
          type: 'Number', format: 'DEC',
          max: 255, min: 1,
        },

        remoteHeader: {
          name: '無線控制 ID',
          description: '使用點播通訊時，可以指定此 ID 控制終端功能，預設值為 0xA8 0x8A，0x00 0x00 表示關閉遠端功能',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },

        ioPin: {
          name: 'GPIO 腳位',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: 'PB14' },
            { val: 1, description: 'PB15' },
            { val: 2, description: 'PC06' },
            { val: 3, description: 'PC07' },
          ],
        },
        ioMode: {
          name: 'GPIO 模式',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '輸出' },
            { val: 1, description: '輸入' },
          ],
        },
        ioLevel: {
          name: 'GPIO 電位',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '低' },
            { val: 1, description: '高' },
            { val: 2, description: '翻轉' },
          ],
        },

        adcPin: {
          name: 'ADC 類比腳位',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0x00, description: 'VDD 電源' },
            { val: 0x01, description: 'PC08' },
            { val: 0x02, description: 'PC09' },
            { val: 0x03, description: 'PC10' },
            { val: 0x04, description: 'PC11' },
          ],
        },
        adcVal: {
          name: 'ADC 轉換數值',
          description: ``,
          type: 'Array', format: 'HEX',
          max: [0x0E, 0x74], min: [0x00, 0x00],
        },

        firmwareVer: {
          name: '韌體版本',
          description: ``,
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF], min: [0x00, 0x00, 0x00],
        },

        // --- custom ---

        floor: {
          name: '樓號',
          description: ``,
          type: 'Number', format: 'DEC',
          max: 255, min: 0,
        },
        room: {
          name: '房號',
          description: ``,
          type: 'Number', format: 'DEC',
          max: 254, min: 0,
        },

        hexMsg: {
          name: '16 進位訊息',
          description: '',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
      },
    },
  },

  'zigbee-matcher': {
    'D01': {
      '1.0.0': {
        p01DeviceType: {
          name: 'Port 01 設備類型',
          description: 'Zigbee 設備種類。更改類型後需重啟模組，設定才能生效',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '協調器' },
            { val: 2, description: '路由器' },
            { val: 3, description: '終端（預設）' },
            { val: 4, description: '休眠終端' },
          ],
        },
        p02DeviceType: {
          name: 'Port 02 設備類型',
          description: 'Zigbee 設備種類。更改類型後需重啟模組，設定才能生效',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '協調器' },
            { val: 2, description: '路由器' },
            { val: 3, description: '終端（預設）' },
            { val: 4, description: '休眠終端' },
          ],
        },

        deviceType: {
          name: '設備類型',
          description: 'Zigbee 設備種類。更改類型後需重啟模組，設定才能生效',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '協調器' },
            { val: 2, description: '路由器' },
            { val: 3, description: '終端（預設）' },
            { val: 4, description: '休眠終端' },
          ],
        },

        floor: {
          name: '樓號',
          description: ``,
          type: 'Number', format: 'DEC',
          max: 255, min: 0,
        },
        room: {
          name: '房號',
          description: ``,
          type: 'Number', format: 'DEC',
          max: 254, min: 0,
        },

        p01Floor: {
          name: 'Port01 樓號',
          description: ``,
          type: 'Number', format: 'DEC',
          max: 255, min: 0,
        },
        p01Room: {
          name: 'Port01 房號',
          description: ``,
          type: 'Number', format: 'DEC',
          max: 254, min: 0,
        },
        p02Floor: {
          name: 'Port02 樓號',
          description: ``,
          type: 'Number', format: 'DEC',
          max: 255, min: 0,
        },
        p02Room: {
          name: 'Port02 房號',
          description: ``,
          type: 'Number', format: 'DEC',
          max: 254, min: 0,
        },


        p01TargetMacAddr: {
          name: 'Port 01 目標 MAC 地址',
          description: '定點通訊模式使用，目標模組 64 bit MAC 地址',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },
        p02LocalMacAddr: {
          name: 'Port 02 設備 MAC 地址',
          description: '模組 64 bit MAC 地址，不可修改',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },


        p01SendMode: {
          name: 'Port 01 發送模式',
          description: '模組之間訊息發送模式',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '廣播（預設）' },
            { val: 1, description: '組播' },
            { val: 2, description: '短地址點播' },
            { val: 3, description: '長地址點播' },
          ],
        },

        ioPin: {
          name: 'GPIO 腳位',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: 'PB14' },
            { val: 1, description: 'PB15' },
            { val: 2, description: 'PC06' },
            { val: 3, description: 'PC07' },
          ],
        },
        ioMode: {
          name: 'GPIO 模式',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '輸出' },
            { val: 1, description: '輸入' },
          ],
        },
        ioLevel: {
          name: 'GPIO 電位',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '低' },
            { val: 1, description: '高' },
            { val: 2, description: '翻轉' },
          ],
        },
      },
    },
  },

  'elevator-ctrler': {
    'circuit-test': {
      '2.0.0': {
        ver: {
          name: '韌體版本',
          description: '3 Bytes，對應 X.X.X',
          type: 'Array', format: 'DEC',
          max: [0xFF, 0xFF, 0xFF], min: [0x00, 0x00, 0x00],
        },

        allOutputPinsLevel: {
          name: '所有 MCP Output 狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '低電位' },
            { val: 1, description: '高電位' },
          ],
        },

        inputPinsLevel: {
          name: '輸入腳位數值',
          description: '二進位由左至右對應 A6、B0、B1、B2、B3 狀態',
          type: 'Number', format: 'BIN',
          max: [0b11111], min: [0b0],
        },

        rtcTime: {
          name: 'RTC 時間',
          description: '',
          type: 'Array',
          format(values) {
            const dateStr = values.map((value) => {
              return String.fromCharCode(value);
            }).join('');

            const Y = dateStr.substring(0, 4);
            const M = dateStr.substring(4, 6);
            const D = dateStr.substring(6, 8);
            const h = dateStr.substring(8, 10);
            const m = dateStr.substring(10, 12);
            const s = dateStr.substring(12, 14);

            return `${Y}-${M}-${D} ${h}:${m}:${s}`;
          },
        },

        romStatus: {
          name: '外部 EEPROM 狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '異常' },
            { val: 255, description: '未初始化' },
            { val: 0xAB, description: '正常' },
          ],
        },
      },
    },
  },

  'qr-reader': {
    'zb02': {
      '1.0.0': {
        ver: {
          name: '韌體版本',
          description: '3 Bytes，對應 X.X.X',
          type: 'Array', format: 'DEC',
          max: [0xFF, 0xFF, 0xFF], min: [0x00, 0x00, 0x00],
        },

        networkStatus: {
          name: '網路狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '無' },
            { val: 1, description: '無可用 SSID' },
            { val: 2, description: '掃描完成' },
            { val: 3, description: '已連線' },
            { val: 4, description: '連線失敗' },
            { val: 5, description: '遺失連線' },
            { val: 6, description: '已斷線' },
            { val: 255, description: '未知狀態' },
          ],
        },

        wifiSsid: {
          name: 'Wi-Fi SSID',
          description: `設定 Wi-Fi 請用下方輸入格，命令格式為 d{S} {P}，S 為任意長度 P 為任意長度密碼，以「空格」分隔，以「.」結尾
例 :
  SSID : key、密碼 : abcd，則輸入 dkey abcd.`,
          type: 'Array',
          format(values) {
            return values.filter((val) => val > 0).map((val) => String.fromCharCode(val)).join('');
          },
          max: [
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
          ],
          min: [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          ],
        },

        wifiPass: {
          name: 'Wi-Fi 密碼',
          description: `設定 Wi-Fi 請用下方輸入格，命令格式為 d{S} {P}，S 為任意長度 P 為任意長度密碼，以「空格」分隔，以「.」結尾
例 :
  SSID : key、密碼 : abcd，則輸入 dkey abcd.`,
          type: 'Array',
          format(values) {
            return values.filter((val) => val > 0).map((val) => String.fromCharCode(val)).join('');
          },
          max: [
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
          ],
          min: [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          ],
        },

        wifiQuality: {
          name: 'Wi-Fi 訊號品質',
          description: '0~100%，無連線則為 255',
          type: 'Number', format: 'DEC',
          max: 0, min: 255,
        },

        ip: {
          name: '目前 IP',
          description: '目前分配 IP，對應 X.X.X.X',
          type: 'Array',
          format(values) {
            return values.join('.');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF], min: [0x00, 0x00, 0x00, 0x00],
        },

        chipId: {
          name: '晶片 ID',
          description: 'ESP8266、ESP32 都有 Chip ID，不足 8 Byte 部分以 0 填充',
          type: 'Array',
          format( /** @type {number[]} */ values) {
            return values.filter((byte) => {
              return byte > 0 ? byte : undefined;
            }).map((byte) => {
              return String.fromCharCode(byte);
            }).join('');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        deviceId: {
          name: '設備 ID',
          description: '設備編號。若為房間設備則可用來判別樓號、房號',
          type: 'Array',
          format( /** @type {number[]} */ values) {
            return values.map((byte) => `${byte}`.padStart(2, '0')).join('-');
          },
          max: [0xFF, 0xFF],
          min: [0x00, 0x00],
        },


        zbType: {
          name: 'Zigbee 節點類型',
          description: 'Zigbee 設備種類。必須為「協調器」',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '協調器' },
            { val: 2, description: '路由器' },
            { val: 3, description: '終端（預設）' },
            { val: 4, description: '休眠終端' },
          ],
        },

        zbPanId: {
          name: 'Pan ID',
          description: 'Zigbee 網路識別符。必需與「設備 ID」相同',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },

        zbRemoteHeader: {
          name: '無線控制 ID',
          description: 'Zigbee 遠端命令 Header。必需為「0xA8 0x8A」',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },

        zbLocalMacAddr: {
          name: '本地 MAC 地址',
          description: '模組 64 bit MAC 地址，不可修改',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        zbTargetMacAddr: {
          name: '目標 MAC 地址',
          description: '必須與「門鎖控制器 MAC」相同',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        zbTargetShortMacAddr: {
          name: '目標短 MAC 地址',
          description: '必須與「門鎖控制器短 MAC」相同',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF],
          min: [0x00, 0x00],
        },

        zbSendMode: {
          name: '發送模式',
          description: '模組之間訊息發送模式。必須為「協議點播」',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '廣播（預設）' },
            { val: 1, description: '組播' },
            { val: 2, description: '短地址點播' },
            { val: 3, description: '長地址點播' },
            { val: 4, description: '協議點播' },
            { val: 5, description: '協議組播' },
          ],
        },

        fixedIp: {
          name: '固定 IP',
          description: '0.0.0.0 表示使用 DHCP',
          type: 'Array', format(values) {
            return values.join('.');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00],
        },

        serverIp: {
          name: '區域伺服器 IP',
          description: '',
          type: 'Array', format(values) {
            return values.join('.');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00],
        },

        engrMode: {
          name: '工程模式',
          description: '開啟工程模式才能進行重要參數設定',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關閉' },
            { val: 1, description: '開啟' },
          ],
        },

        rtcDateTime: {
          name: 'RTC 時間',
          description: '',
          type: 'Array',
          format( /** @type {number[]} */ values) {
            return values.filter((byte) => {
              return byte > 0 ? byte : undefined;
            }).map((byte) => {
              return String.fromCharCode(byte);
            }).join('');
          },
        },

        zbConnected: {
          name: '門鎖控制器連線狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '未連線' },
            { val: 1, description: '連線成功' },
          ],
        },

        mqttConnected: {
          name: 'MQTT 連線狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '未連線' },
            { val: 1, description: '連線成功' },
          ],
        },

        wsBright: {
          name: '燈條亮度',
          description: '',
          type: 'Number', format: 'DEC',
          max: 255,
          min: 0,
        },

        wsPlayType: {
          name: '燈條閃爍樣式',
          description: '用於反饋問題與情境之閃爍樣式',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '拒絕' },
            { val: 1, description: '成功' },
            { val: 2, description: '請重試' },
            { val: 3, description: '警告' },
            { val: 4, description: '低電量' },
          ],
        },

        doorCtrlerBatVal: {
          name: '門鎖控制器電池電量',
          description: '低於 5.1V 表示低電量',
          type: 'Array',
          format( /** @type {number[]} */ values) {
            const number = values[0] << 8 | values[1];
            const vol = number * 6.4 / 3700;
            return vol;
          },
          max: [0xFF, 0xFF],
          min: [0x00, 0x00],
        },

        beepPlayType: {
          name: '蜂鳴器曲目',
          description: '用於反饋問題與情境之閃爍樣式',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '成功' },
            { val: 1, description: '請重試' },
            { val: 3, description: '拒絕' },
            { val: 4, description: '無回應' },
            { val: 5, description: '系統設定' },
          ],
        },

        powerRelayStatus: {
          name: '節電器繼電器狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '通導' },
          ],
        },

        murStatus: {
          name: '掃房燈狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '發亮' },
          ],
        },

        dndStatus: {
          name: '勿擾燈狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '發亮' },
          ],
        },

        roomServiceLedType: {
          name: '房間燈號種類',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '掃房燈' },
            { val: 2, description: '勿擾燈' },
          ],
        },

        ledStatus: {
          name: '燈號狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '發亮' },
          ],
        },
      },
    },
    'generic': {
      '3.3.0': {
        ver: {
          name: '韌體版本',
          description: '3 Bytes，對應 X.X.X',
          type: 'Array', format: 'DEC',
          max: [0xFF, 0xFF, 0xFF], min: [0x00, 0x00, 0x00],
        },

        networkStatus: {
          name: '網路狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '無' },
            { val: 1, description: '無可用 SSID' },
            { val: 2, description: '掃描完成' },
            { val: 3, description: '已連線' },
            { val: 4, description: '連線失敗' },
            { val: 5, description: '遺失連線' },
            { val: 6, description: '已斷線' },
            { val: 255, description: '未知狀態' },
          ],
        },

        wifiSsid: {
          name: 'Wi-Fi SSID',
          description: `設定 Wi-Fi 請用下方輸入格，命令格式為 d{S} {P}，S 為任意長度 P 為任意長度密碼，以「空格」分隔，以「.」結尾
例 :
  SSID : key、密碼 : abcd，則輸入 dkey abcd.`,
          type: 'Array',
          format(values) {
            return values.filter((val) => val > 0).map((val) => String.fromCharCode(val)).join('');
          },
          max: [
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
          ],
          min: [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          ],
        },

        wifiPass: {
          name: 'Wi-Fi 密碼',
          description: `設定 Wi-Fi 請用下方輸入格，命令格式為 d{S} {P}，S 為任意長度 P 為任意長度密碼，以「空格」分隔，以「.」結尾
例 :
  SSID : key、密碼 : abcd，則輸入 dkey abcd.`,
          type: 'Array',
          format(values) {
            return values.filter((val) => val > 0).map((val) => String.fromCharCode(val)).join('');
          },
          max: [
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
          ],
          min: [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          ],
        },

        wifiQuality: {
          name: 'Wi-Fi 訊號品質',
          description: '0~100%，無連線則為 255',
          type: 'Number', format: 'DEC',
          max: 255, min: 0,
        },

        ip: {
          name: '目前 IP',
          description: '目前分配 IP，對應 X.X.X.X',
          type: 'Array',
          format(values) {
            return values.join('.');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF], min: [0x00, 0x00, 0x00, 0x00],
        },

        chipId: {
          name: '晶片 ID',
          description: 'ESP8266、ESP32 都有 Chip ID，不足 8 Byte 部分以 0 填充',
          type: 'Array',
          format( /** @type {number[]} */ values) {
            return values.filter((byte) => {
              return byte > 0 ? byte : undefined;
            }).map((byte) => {
              return String.fromCharCode(byte);
            }).join('');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        deviceId: {
          name: '設備 ID',
          description: '設備編號。若為房間設備則可用來判別樓號、房號',
          type: 'Array',
          format( /** @type {number[]} */ values) {
            return values.map((byte) => `${byte}`.padStart(2, '0')).join('-');
          },
          max: [0xFF, 0xFF],
          min: [0x00, 0x00],
        },
        deviceLevel: {
          name: '設備等級',
          description: '房間設備必須為 0x22（34），公共空間為 0x10（16）',
          type: 'Number', format: 'HEX',
          max: 255, min: 0,
        },

        zbType: {
          name: 'Zigbee 節點類型',
          description: 'Zigbee 設備種類。必須為「協調器」',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '協調器' },
            { val: 2, description: '路由器' },
            { val: 3, description: '終端（預設）' },
            { val: 4, description: '休眠終端' },
          ],
        },

        zbPanId: {
          name: 'Pan ID',
          description: 'Zigbee 網路識別符。必需與「設備 ID」相同',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },

        zbRemoteHeader: {
          name: '無線控制 ID',
          description: 'Zigbee 遠端命令 Header。必需為「0xA8 0x8A」',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },

        zbLocalMacAddr: {
          name: '本地 MAC 地址',
          description: '模組 64 bit MAC 地址，不可修改',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        zbTargetMacAddr: {
          name: '目標 MAC 地址',
          description: '必須與「門鎖控制器 MAC」相同',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        zbTargetShortMacAddr: {
          name: '目標短 MAC 地址',
          description: '必須與「門鎖控制器短 MAC」相同',
          type: 'Array', format: 'HEX',
          max: [0xFF, 0xFF],
          min: [0x00, 0x00],
        },

        zbSendMode: {
          name: '發送模式',
          description: '模組之間訊息發送模式。必須為「協議點播」',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '廣播（預設）' },
            { val: 1, description: '組播' },
            { val: 2, description: '短地址點播' },
            { val: 3, description: '長地址點播' },
            { val: 4, description: '協議點播' },
            { val: 5, description: '協議組播' },
          ],
        },

        fixedIp: {
          name: '固定 IP',
          description: '0.0.0.0 表示使用 DHCP',
          type: 'Array', format(values) {
            return values.join('.');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00],
        },

        serverIp: {
          name: '區域伺服器 IP',
          description: '',
          type: 'Array', format(values) {
            return values.join('.');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00],
        },

        engrMode: {
          name: '工程模式',
          description: '開啟工程模式才能進行重要參數設定',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關閉' },
            { val: 1, description: '開啟' },
          ],
        },

        rtcDateTime: {
          name: 'RTC 時間',
          description: '',
          type: 'Array',
          format( /** @type {number[]} */ values) {
            return values.filter((byte) => {
              return byte > 0 ? byte : undefined;
            }).map((byte) => {
              return String.fromCharCode(byte);
            }).join('');
          },
        },

        zbConnected: {
          name: '門鎖控制器連線狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '未連線' },
            { val: 1, description: '連線成功' },
          ],
        },

        mqttConnected: {
          name: 'MQTT 連線狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '未連線' },
            { val: 1, description: '連線成功' },
          ],
        },

        wsBright: {
          name: '燈條亮度',
          description: '',
          type: 'Number', format: 'DEC',
          max: 255,
          min: 0,
        },

        wsPlayType: {
          name: '燈條閃爍樣式',
          description: '用於反饋問題與情境之閃爍樣式',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '拒絕' },
            { val: 1, description: '成功' },
            { val: 2, description: '請重試' },
            { val: 3, description: '警告' },
            { val: 4, description: '低電量' },
          ],
        },

        doorCtrlerBatVal: {
          name: '門鎖控制器電池電量',
          description: '低於 5.1V 表示低電量',
          type: 'Array',
          format( /** @type {number[]} */ values) {
            const number = values[0] << 8 | values[1];
            const vol = number * 6.4 / 3700;
            return vol;
          },
          max: [0xFF, 0xFF],
          min: [0x00, 0x00],
        },

        beepPlayType: {
          name: '蜂鳴器曲目',
          description: '用於反饋問題與情境之閃爍樣式',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '成功' },
            { val: 1, description: '請重試' },
            { val: 3, description: '拒絕' },
            { val: 4, description: '無回應' },
            { val: 5, description: '系統設定' },
          ],
        },

        powerRelayStatus: {
          name: '節電器繼電器狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '通導' },
          ],
        },

        murStatus: {
          name: '掃房燈狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '發亮' },
          ],
        },

        dndStatus: {
          name: '勿擾燈狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '發亮' },
          ],
        },

        roomServiceLedType: {
          name: '房間燈號種類',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 1, description: '掃房燈' },
            { val: 2, description: '勿擾燈' },
          ],
        },

        ledStatus: {
          name: '燈號狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '發亮' },
          ],
        },

        extEnable: {
          name: '外部模組啟用設定',
          description: '從最低位元（最右邊）開始，1：ZB 無線模組、2：QR Code 辨識模組',
          type: 'Number', format(values) {
            return parseInt(values).toString(2).padStart(8, '0');
          },
          max: 255,
          min: 0,
        },

        enableValue: {
          name: '開關數值',
          description: 'cmd 命令用',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '關' },
            { val: 1, description: '開' },
          ],
        },

        relaySecond: {
          name: 'Relay 動作秒數',
          description: '0 表示永久動作',
          type: 'Number', format: 'DEC',
          max: 127,
          min: 0,
        },

        relayLogicalNot: {
          name: 'Relay 反向邏輯設定',
          description: '設定繼電器如何動作',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '正向邏輯' },
            { val: 1, description: '反向邏輯' },
          ],
        },
      },
    },
  },

  'df-guard': {
    'D01': {
      '1.0.0': {
        ver: {
          name: '韌體版本',
          description: '3 Bytes，對應 X.X.X',
          type: 'Array', format: 'DEC',
          max: [0xFF, 0xFF, 0xFF], min: [0x00, 0x00, 0x00],
        },

        networkStatus: {
          name: '網路狀態',
          description: '',
          type: 'Number', format: 'DEC',
          list: [
            { val: 0, description: '無' },
            { val: 1, description: '無可用 SSID' },
            { val: 2, description: '掃描完成' },
            { val: 3, description: '已連線' },
            { val: 4, description: '連線失敗' },
            { val: 5, description: '遺失連線' },
            { val: 6, description: '已斷線' },
            { val: 255, description: '未知狀態' },
          ],
        },

        wifiSsid: {
          name: 'Wi-Fi SSID 名稱',
          description: '',
          type: 'Array',
          format(values) {
            return values.filter((val) => val > 0).map((val) => String.fromCharCode(val)).join('');
          },
          max: [
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
          ],
          min: [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          ],
        },

        wifiQuality: {
          name: 'Wi-Fi 訊號品質',
          description: '0~100%，無連線則為 255',
          type: 'Number', format: 'DEC',
          max: 0, min: 255,
        },

        ip: {
          name: '目前 IP',
          description: '目前分配 IP，對應 X.X.X.X',
          type: 'Array',
          format(values) {
            return values.join('.');
          },
          max: [0xFF, 0xFF, 0xFF, 0xFF], min: [0x00, 0x00, 0x00, 0x00],
        },
      },
    },
  },
};

