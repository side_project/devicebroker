module.exports = {
  'env': {
    'commonjs': true,
    'es2021': true,
    'node': true,
    'jest/globals': true,
  },
  'extends': [
    'eslint:recommended',
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 12,
  },
  'parser': 'babel-eslint',
  'plugins': [
    'jest',
  ],
  'globals': {
    'Vue': true,
    'VueI18n': true,
    'Quasar': true,
    'moment': true,
    'document': true,
    'Mousetrap': true,
    'window': true,
  },
  'rules': {
    // 函數前括號空格
    'space-before-function-paren': ['error', {
      'anonymous': 'always',
      'named': 'never',
      'asyncArrow': 'always',
    }],

    'object-curly-spacing': ['error', 'always'],

    'max-len': ['error', {
      'ignoreStrings': true,
      'ignoreTemplateLiterals': true,
      'ignoreUrls': true,
      'ignoreRegExpLiterals': true,
      'ignoreComments': true,
      'ignoreTrailingComments': true,
    }],
  },
};
